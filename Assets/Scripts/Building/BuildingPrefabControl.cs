﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class BuildingPrefabControl : NetworkBehaviour
{

    [SyncVar]
    GameObject playerManager;
    [SyncVar]
    public bool autoCreation;
    [SyncVar]
    public int buildingLevel;
    [SyncVar]
    public int buildingCost;
    [SyncVar]
    public int buildingCreationTime;

    public GameObject ghostBuilding;
    public GameObject building;

    [SyncVar]
    [HideInInspector] public bool buildingIsCreated = false;

	[Command]
	public void CmdInstantiateBuildingObject() {
		GameObject buildingGo = (GameObject)Instantiate(Resources.Load(building.name, typeof(GameObject)), transform.position, Quaternion.identity);
		BuildingControl buildingControl = buildingGo.GetComponent<BuildingControl> ();
		buildingControl.playerManagerGo = playerManager;
		buildingControl.startBuildingLevel = buildingLevel;
		buildingControl.PrepareBuilding ();
		//bfc.PrepareCreationBuilding(this.gameObject, level, autoCreation);
		//NetworkServer.SpawnWithClientAuthority(buildingGo, this.gameObject.GetComponent<NetworkIdentity>().connectionToClient);
		NetworkServer.SpawnWithClientAuthority(buildingGo, playerManager);
        Destroy(this.gameObject);

        // NetworkServer.Destroy(this.gameObject); Если надумаешь это вернуть, то в CreatingBuilding надо убрать Destroy(gameObject), т.к. его уже не существует
	}


    /// <summary>
    /// А вот это уже вызывается на клиентах, тут настройки для ВСЕХ клиентов. Они должны быть синхронизированы с сервером
    /// </summary>
    public override void OnStartClient() {

        base.OnStartClient();

        PlayerManager pm = playerManager.GetComponent<PlayerManager>();
        gameObject.layer = pm.playerLayer;
        ghostBuilding.layer = pm.playerLayer;
        ghostBuilding.GetComponent<CreatingBuilding>().playerManager = pm;
    }

    /// <summary>
    /// Вызывается на авторизированном объекте на клиенте. Вызывается после  
    /// OnStartClient() на клиенте
    /// </summary>
    public override void OnStartAuthority() {
        base.OnStartAuthority();
        ghostBuilding.SetActive(true);
    }

    /// <summary>
    /// Эта хрень вызывается на сервере. Поэтому надо настроить объекты !НА СЕРВЕРЕ. К клиенской части это отношения не имеет.
    /// </summary>
    /// <param name="_playerManager">Player manager.</param>
    /// <param name="level">Уровень здания, которое собирается строится. Будут учтены апгрейды.</param>
    /// <param name="_autoCreation">If set to <c>true</c> auto creation.</param>
    public void PrepareCreationBuilding(GameObject _playerManager, int level, bool _autoCreation) {
        playerManager = _playerManager;
        autoCreation = _autoCreation;
        buildingLevel = level;
        PlayerManager pm = playerManager.GetComponent<PlayerManager>();
        gameObject.layer = pm.playerLayer;
        ghostBuilding.layer = pm.playerLayer;
    }

    // Возможно надо перелавать необходимый уровень в параметрах
    public int GetAdditionCostForLevel(int level) {
        int cost = 0;
        for (int i = 0; i <= level; i++) {
            cost += building.GetComponent<BuildingLevelUpSystem>().levels[i].cost;
        }

        return cost;
    }

	// !!! Этого здесь быть не должно. Перенеси в BuildingControl
    //public int GetLevelBuildingCost(int level) {
    //    return GetComponent<BuildingLevelUpSystem>().levels[level].cost;
    //}
}