﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

public class BuildingHealth : NetworkBehaviour, IHealth
{

    [HideInInspector]
    public PlayerManager playerManager;
    private Slider healthSlider;
	public float startHealth = 100;
	[SyncVar(hook = "OnChangeCurrentHealth")]
	public int currentHealth;                   // The current health the enemy has.
    public int AdditionalCostForShoot { get; set; }

    private bool isDestroy = false;                                // Whether the enemy is dead.
    private SphereCollider sphereCollider;            // Reference to the capsule collider.

	/// <summary>
	/// Отвечает за изменения слайдера на клиентах при изменении currentHealth
	/// </summary>
	/// <param name="currentHealth">Current health.</param>
	void OnChangeCurrentHealth(int currentHealth) {
        healthSlider.value = currentHealth;
	}

    // Use this for initialization
    public override void OnStartClient() {
        healthSlider = GetComponentInChildren<Slider>();
        // Setting up the references.
        //healthSlider = GetComponentInChildren<Slider>();
        //anim = GetComponent<Animator>();
        //enemyAudio = GetComponent<AudioSource>();
        //hitParticles = GetComponentInChildren<ParticleSystem>();
        sphereCollider = GetComponent<SphereCollider>();

		healthSlider.maxValue = startHealth;
        healthSlider.value = currentHealth;

    }

    public override void OnStartServer() {
        base.OnStartServer();
        sphereCollider = GetComponent<SphereCollider>();
    }

    public bool IsDestroyed() {
        return isDestroy;
    }

    /// <summary>
    /// Это исполняется на сервере, но т.к. через RayCast мы получаем BuildingObject, то у него нет NetworkIdentity
    /// поэтому сам параметр currentHealth хранится в BuildingPrefabControl. Там он и синхронизирован и Hook на нем для изменения слайдераS
    /// </summary>
    /// <param name="amount">Amount.</param>
    /// <param name="hitPoint">Hit point.</param>
    public void TakeDamage(int amount, Vector3 hitPoint) {

        if (!isServer)
        	return;
        // If the enemy is dead...
        if (isDestroy)
            // ... no need to take damage so exit the function.
            return;

        // Play the hurt sound effect.
        //enemyAudio.Play();

        // Reduce the current health by the amount of damage sustained.
        currentHealth -= amount;

        // Чтобы небыло ошибок, надо проинициализировать healthSlider на сервере
        // healthSlider.value = currentHealth;

        // Set the position of the particle system to where the hit was sustained.
        //hitParticles.transform.position = hitPoint;

        // And play the particles.
        //hitParticles.Play();

        // If the current health is less than or equal to zero...
        if (currentHealth <= 0) {
            // ... the enemy is dead.
            DestroyBuilding();
        }
    }

	// Это вызывается и на сервере и на клиенте (из buildingPrefabControl)
    public void DestroyBuilding() {
        // The enemy is dead.

        //playerManager.playerMap.DestroyBuilding((int)transform.position.x, (int)transform.position.z);

        isDestroy = true;

        // Turn the collider into a trigger so shots can pass through it.
        sphereCollider.isTrigger = true;

        // Tell the animator that the enemy is dead.
        //anim.SetTrigger("Dead");

        // Change the audio clip of the audio source to the death clip and play it (this will stop the hurt clip playing).
        //enemyAudio.clip = deathClip;
        //enemyAudio.Play();

        //gameObject.GetComponent<UnitControl>().deleteUnit();        // !!!!!! просто грязь
		NetworkServer.Destroy(this.gameObject);
    }
}
