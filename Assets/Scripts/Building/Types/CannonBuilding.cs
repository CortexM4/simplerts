﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Linq;

public class CannonBuilding : BuildingControl, IShootable, IEnemyVisible {
                                                         //
    //!!!!!!!! Надо выяснить насчет расстояния, т.к. при 1 юнит не может достичь цели и тупит !!!!!!!!!!!
    public float shootDistance = 6;          // Это расстояние с которого начинаем стрелять    // 
    public float shootRate = 1f;                                                               //
	public ParticleSystem gunParticles;                    // Reference to the particle system.
    //--------------------------------------------------------------------------------------------

	public float updateTimeRotation = 0.5f;

    //-------------- Это относится к классу UnitAttack ---------------------
    
    public int damagePerShot = 10;
    public float range = 100f;                              // The distance the gun can fire. А это, как далеко полетит снаряд (для рукопашки = shootDistance)
    private Ray shootRay;                                   // A ray from the gun end forwards.
    private RaycastHit shootHit;                            // A raycast hit to get information about what was hit.
    //----------------------------------------------------------------------

    private List<Aim> aims = new List<Aim>();

    private float nextFire;
	private LayerMask enemyLayer;
    private LayerMask layerMaskEnemy;
	private BuildingHealth buildingHealth;

	public override void PrepareBuilding() {
		base.PrepareBuilding();
		buildingHealth = GetComponent<BuildingHealth> ();
		enemyLayer = playerManager.enemyLayer;
		layerMaskEnemy = 1 << enemyLayer;
        GetComponentInParent<TransformSync>().SyncRotation();
    }

    [ServerCallback]
    void Update() {
        if (!isServer)
            return;

        if (buildingHealth.IsDestroyed())
            return;

        if (Time.time > nextFire) {
            Aim tmp_aim = GetComponent<SelectAim>().GetPrimaryAim();

            if (tmp_aim != null) {
                EnemyFound(tmp_aim.enemy);
            }
        }
	}

    void EnemyFound(GameObject aim) {
        GetComponentInParent<TransformSync>().LookAt(aim.transform);
        if (Vector3.Distance(transform.position, aim.transform.position) <= shootDistance) {
            Vector3 dirToShoot = aim.transform.position - transform.position;                    // если расстояние позволяет стрелять, то
            if (Time.time > nextFire) {                                                                 // стреляем с определенным Rate
                nextFire = Time.time + shootRate;
                Shoot(dirToShoot);
            }
        }
    }

// ================================ IShootable интерфейс ========================================
    public List<Aim> GetAims() {
        return aims;
    }
    
    public float GetShootDistance() {
        return shootDistance;
    }

    public void Shoot(Vector3 shootDir) {
        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if (Physics.Raycast(shootRay, out shootHit, range, layerMaskEnemy)) {

            IHealth targetHealth = shootHit.collider.GetComponent<IHealth>();

            // If the EnemyHealth component exist...
            if (targetHealth != null) {
                // ... the enemy should take damage.
                targetHealth.TakeDamage(damagePerShot, shootHit.point);
            }
        }

        // ???? Что делать если мы не попадаем или какое-то препятствие на пути

        RpcGunShootParticles();
        gunParticles.Stop();
        gunParticles.Play();
    }
// ==========================================================================================================

    [ClientRpc]
    void RpcGunShootParticles() {
        gunParticles.Stop();
        gunParticles.Play();
    }

// =============================================================================================

    public override void SelectedBuilding() {
		DrawCircle dc = GetComponentInChildren<DrawCircle>();
		if (dc != null)
			dc.DrawEnable();
    }

    public override void UnselectedBuilding() {
		DrawCircle dc = GetComponentInChildren<DrawCircle>();
		if (dc != null)
			dc.DrawDisable();
    }

// ==================================== IEnemyVisible интерфейс =============================================	
	public void ReactionOnEnemy (Collider collider) {
        if (collider.gameObject.layer == enemyLayer) {
            IHealth targetHealth = collider.gameObject.GetComponentInChildren<IHealth>();
            if (targetHealth != null) {
                if (aims.Find(e => e.enemy == collider.gameObject) == null) {
                    Aim aim = new Aim(collider.gameObject);
                    aims.Add(aim);
                }
            }
        }
	}
// ==========================================================================================================
}
