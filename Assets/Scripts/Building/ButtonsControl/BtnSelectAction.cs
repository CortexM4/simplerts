﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BtnSelectAction : MonoBehaviour, IButtonAccess
{

	public ActionControl actionPrefab;
    public Color buttonColor;

    public Image buttonImage;
    public Color actionColor;

    private Sprite spriteImage;
    private PlayerManager playerManager;
    private Image btnColor;
    private Button button;
    private int techCost;

	public void EnableButton(BuildingControl buildingControl) {
		if (actionPrefab == null) {
            Debug.LogError(gameObject.name +": Assign techPrefab");
            return;
        }
        if (buttonImage == null) {
            Debug.LogError(gameObject.name + ": Assign buttonImage");
            return;
        }
		techCost = actionPrefab.GetActionCost();
		playerManager = buildingControl.GetComponent<IPlayer>().PlayerManager;

        btnColor = GetComponent<Image>();
        
        btnColor.color = buttonColor;
        button = GetComponent<Button>();
        button.interactable = true;
        spriteImage = buttonImage.sprite;
        StartCoroutine("CheckTechCost");
    }
    //--------- Вызывается при нажатии кнопки создания юнита
    public void SelectTechnology() {
		playerManager.GetHUD().SelecetAction(spriteImage, actionColor, actionPrefab);

    }

    // Проверка конопки. Если денег не хватает, то ее отключаем
    // иначе, включаем
    IEnumerator CheckTechCost() {
        while (true) {
            if (techCost <= playerManager.currentMoney && !button.IsInteractable())
                button.interactable = true;
            if (techCost > playerManager.currentMoney && button.IsInteractable())
                button.interactable = false;
                
            yield return new WaitForSeconds(.1f);
        }
    }
}
