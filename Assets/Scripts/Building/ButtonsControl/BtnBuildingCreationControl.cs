﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BtnBuildingCreationControl : MonoBehaviour, IButtonAccess {

    public GameObject buildingPrefab;
    public int buildingPrefabLevel;
    public Color buttonColor;

    private PlayerManager playerManager;
    private Image btnColor;
    private Button button;
    private int buildingCost;
    private int levelCost;


	public void EnableButton(BuildingControl buildingControl) {
        if (buildingPrefab == null) {
            Debug.LogError(gameObject.name +": Assign buildingPrefab");
            return;
        }
		playerManager = buildingControl.GetComponent<IPlayer>().PlayerManager;
        buildingCost = buildingPrefab.GetComponent<BuildingPrefabControl>().buildingCost;                        // Стоимость самого здания
        levelCost = buildingPrefab.GetComponent<BuildingPrefabControl>().GetAdditionCostForLevel(buildingPrefabLevel);   // Стоимость уровня здания

        button = GetComponent<Button>();
        button.interactable = true;
        btnColor = GetComponent<Image>();
        btnColor.color = buttonColor;
		StartCoroutine("CheckBuildingCost");
    }

    public void CreateBuilding() {
        BuildingControl buildingControl = transform.GetComponentInParent<BuildingControl>();
        buildingControl.UnselectedBuilding();
        playerManager.CreateMyBuilding(buildingPrefab, buildingPrefabLevel);
    }

    // Проверка конопки. Если денег не хватает, то ее отключаем
    // иначе, включаем
    IEnumerator CheckBuildingCost() {
        while (true) {
            if (buildingCost + levelCost <= playerManager.currentMoney && !button.IsInteractable())
                button.interactable = true;
            if (buildingCost + levelCost > playerManager.currentMoney && button.IsInteractable())
                button.interactable = false;

            yield return new WaitForSeconds(.1f);
        }
    }
}
