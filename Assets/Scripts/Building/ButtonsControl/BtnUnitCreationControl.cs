﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BtnUnitCreationControl : MonoBehaviour, IButtonAccess {

    public UnitControl unitPrefab;
    public Color buttonColor;
    public Color sliderColor;
    public Image backgroundSlider;


    private PlayerManager playerManager;
    private Vector3 buildingCoord;
    private Slider creationSlider;
    private Image btnColor;
    private Button button;
    private int unitCost;

    // !!!!!!! Заметка !!!!!!!!!!
    // чтобы кнопка была кликабельна, надо в слайдере background и fill снять галочки Raycast !!!!!!!!!!
	public void EnableButton(BuildingControl buildingControl) {
        if (unitPrefab == null) {
            Debug.LogError("Assign unitPrefab");
            return;
        }
        unitCost = unitPrefab.GetCostForUnit();
		playerManager = buildingControl.GetComponent<IPlayer>().PlayerManager;
		buildingCoord = buildingControl.transform.position;

        btnColor = GetComponent<Image>();
        creationSlider = GetComponentInChildren<Slider>();
        
        btnColor.color = buttonColor;
        backgroundSlider.color = sliderColor;
        button = GetComponent<Button>();
        button.interactable = true;
        StartCoroutine("CheckUnitCost");
    }
    //--------- Вызывается при нажатии кнопки создания юнита
    public void CreateUnit() {
        StopCoroutine("CheckUnitCost");
        button.interactable = false;
        playerManager.CmdDecreaseMoneyOnServer(unitCost);
        creationSlider.maxValue = unitPrefab.GetTimeUnitCreation();
        creationSlider.value = 0;
        StartCoroutine("CreateUnitCoroutine", unitPrefab);
    }

    // Проверка конопки. Если денег не хватает, то ее отключаем
    // иначе, включаем
    IEnumerator CheckUnitCost() {
        while (true) {
            if (unitCost <= playerManager.currentMoney && !button.IsInteractable())
                button.interactable = true;
            if (unitCost > playerManager.currentMoney && button.IsInteractable())
                button.interactable = false;
                
            yield return new WaitForSeconds(.1f);
        }
    }

    // Отвечает за постройку юнита. Время постройки индицирует slider
    IEnumerator CreateUnitCoroutine(UnitControl unitPrefab) {
        float time = Time.time + unitPrefab.GetTimeUnitCreation();
        while (Time.time < time) {
            creationSlider.value += Time.deltaTime;
            yield return null;
        }
        creationSlider.value = 0;

        playerManager.CmdCreateUnit(unitPrefab.name, new Vector3(buildingCoord.x, 0, buildingCoord.z) + new Vector3(0, 0, -2));
        button.interactable = true;
        StartCoroutine("CheckUnitCost");
    }
}
