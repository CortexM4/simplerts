﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BuildingLevelUpSystem : MonoBehaviour {

    public int levelMainBuildingFirstCreation = 0;
    public Text infoText;
    public Button levelUpButton;
    public List<BuildingLevel> levels;

    private string text = "Building level: ";
    private BuildingLevel currentLevel;

    /// <summary>
    /// Событие вызывается при нажатии кнопки повышения уровня у здания
    /// !ВАЖНО: CmdIncreaseMainBuilding выполняется на сервере. Списание денег должно быть с проверкой на сервере тоже
    /// ! для того, чтобы занть цену необходимо List<BuilsingLevel> тоже хранить не сервере. Иначе клиент может фальсифицировать данные
    /// </summary>
    public void btnEvent_LevelUp() {
        currentLevel = GetNextItem(levels, currentLevel);
        if (currentLevel != null) {
            GetComponent<IPlayer>().PlayerManager.CmdDecreaseMoneyOnServer(currentLevel.cost);
			if (GetComponent<BuildingControl> () is MainBuilding) {
				GetComponent<IPlayer>().PlayerManager.CmdIncreaseMainBuildingLevel();
			}
            infoText.text = text + currentLevel.level.ToString();
            //BuildingControl bc = GetComponent<BuildingControl>();
            //if (bc is MainBuilding) {
            //   MainBuilding mb = (MainBuilding)bc;
            //   mb.CmdIncreaseBuildingLevel();
            //}

            foreach (Button btn in currentLevel.buttons) {
				btn.GetComponent<IButtonAccess>().EnableButton(GetComponent<BuildingControl>());
            }
        }
    }

	/// <summary>
	/// Sets the building level.
	/// Метод вызывается при строении здания
	/// При нажатии кноки Accept списывается стоимость здания и все его стоимости повышения уровней до необходимого включительно
	/// </summary>
	/// <param name="level">Level.</param>
	public void SetBuildingLevel(int level) {
		for (int i = 0; i <= level; i++) {
			currentLevel = levels [i];
			if (currentLevel != null) {
				infoText.text = text + currentLevel.level.ToString();

				foreach (Button btn in currentLevel.buttons) {
					btn.GetComponent<IButtonAccess>().EnableButton(GetComponent<BuildingControl>());
				}
			}
		}

        StartCoroutine("CheckLevelUpBtn");
        
    }

    IEnumerator CheckLevelUpBtn() {
        BuildingControl buildingControl = GetComponent<BuildingControl>();
        while (true) {
            if (GetNextItem(levels, currentLevel) == null) {                // Если следуюший элемент null, то последний upgrade был сделан
                levelUpButton.interactable = false;                            // кнопку можно отключить
                break;
            }

            if (buildingControl is MainBuilding) {

                if (GetNextItem(levels, currentLevel).cost <= GetComponent<IPlayer>().PlayerManager.currentMoney && !levelUpButton.IsInteractable())
                    levelUpButton.interactable = true;
                if (GetNextItem(levels, currentLevel).cost > GetComponent<IPlayer>().PlayerManager.currentMoney && levelUpButton.IsInteractable())
                    levelUpButton.interactable = false;
            }
            else {
                if (currentLevel.level < (GetComponent<IPlayer>().PlayerManager.levelMainBuilding - levelMainBuildingFirstCreation)) {
                    if (GetNextItem(levels, currentLevel).cost <= GetComponent<IPlayer>().PlayerManager.currentMoney && !levelUpButton.IsInteractable())
                        levelUpButton.interactable = true;
                    if (GetNextItem(levels, currentLevel).cost > GetComponent<IPlayer>().PlayerManager.currentMoney && levelUpButton.IsInteractable())
                        levelUpButton.interactable = false;
                }
                else
                    levelUpButton.interactable = false;
            }
            yield return null;
        }
    }

    public BuildingLevel GetNextItem(List<BuildingLevel> list, BuildingLevel item) {
        if(list.IndexOf(item)+1 == list.Count)
            return null;

        //return list[(list.IndexOf(item) + 1) == list.Count ? 0 : (list.IndexOf(item) + 1)];
        return list[(list.IndexOf(item) + 1)];
    }


}

[System.Serializable]
public class BuildingLevel
{
    public int level;
    public int cost;
    public List<Button> buttons;
}
