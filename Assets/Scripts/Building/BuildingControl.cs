﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking;

/*
 *  Класс, реализует поведение MonoBehaviour
 *  наследуется отдельными экземплярами для расширения
 */
public class BuildingControl : NetworkBehaviour, IVisibility, IPlayer
{
    [SyncVar]
    public int startBuildingLevel;
	[SyncVar]
	[HideInInspector]public GameObject playerManagerGo;
	[HideInInspector]public PlayerManager playerManager;
    // Реализация интерфейса IPlayer, для облегчения доступа
    [HideInInspector]public PlayerManager PlayerManager { get { return playerManager; } }
    // Канва кнопок
    public Canvas menuCanvas;

    public Material defaultMaterial;

    //private Canvas _buildingCanvas;

    private Renderer rend;
    private Color defaultColor;
    private Material mat;


	public override void OnStartClient ()
	{
        base.OnStartClient();
		playerManager = playerManagerGo.GetComponent<PlayerManager> ();
		gameObject.layer = playerManager.playerLayer;

		rend = GetComponentInChildren<Renderer>();
		defaultColor = playerManager.playerColor;
		mat = rend.material;
		mat.color = defaultColor;

		GetComponent<BuildingLevelUpSystem>().SetBuildingLevel(startBuildingLevel);

		MiniMapControl.Instance.EnableBuildingIcon(transform);
		
	}

	public override void OnNetworkDestroy() {
		if (!isClient)
			return;
		playerManager.DeleteCreationPanel(transform.position);
		MiniMapControl.Instance.DisableBuildingIcon(transform);
	}

    //----------- Всякие первичные настройки ------------
	public virtual void PrepareBuilding() {

		playerManager = playerManagerGo.GetComponent<PlayerManager> ();
        rend = GetComponentInChildren<Renderer>();

		gameObject.layer = playerManager.playerLayer;
        rend.material = defaultMaterial;
        defaultColor = playerManager.playerColor;
        mat = rend.material;
        mat.color = defaultColor;
        
        //GetComponent<BuildingHealth>().playerManager = playerManager;
        //GetComponent<BuildingHealth>().HealthReset();

    }

    // Включаем канву меню здания, потом ищем первую дочернюю канву и включаем ее
    public virtual void SelectedBuilding() {
        //Drawing.DrawCircle(new Vector2(transform.position.x, transform.position.z), GetComponentInParent<NetworkVisibleChecker>().visRange, Color.green, 5, 20);
        CameraManager.Instance.isBuildingMenuOpen = true;
        menuCanvas.enabled = true;
        Canvas[] canvases = menuCanvas.gameObject.GetComponentsInChildren<Canvas>();
        foreach (Canvas canvas in canvases) {
            if (canvas != menuCanvas) {
                canvas.enabled = true;
                break;
            }
        }
        //canvasControl.SetActive(true);
        //_buildingCanvas.enabled = true;
        //buildigControlPanel.SetActive (true);
    }

    // Это просто треш. Если отключить канву(MenuMainBuilding), но при этом не отключить дочерние канвы,
    // то дочерние, если они включени смещаются меняя якоря.
    // Для решения я выключаю все дочерние канвы перед отключением основной (Menu)
    // и только после это основную
    public virtual void UnselectedBuilding() {
        CameraManager.Instance.isBuildingMenuOpen = false;
        Canvas[] canvases = menuCanvas.gameObject.GetComponentsInChildren<Canvas>();
        foreach (Canvas canvas in canvases) {
            if (canvas != menuCanvas)
                canvas.enabled = false;
        }
        menuCanvas.enabled = false;
    }

    public bool IsVisible() {
        return true;
    }
}
