﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreatingBuilding : MonoBehaviour
{

    // PlayManager
    [HideInInspector] public PlayerManager playerManager;

    public Material ghostMaterial;

    private BuildingPrefabControl bpc;
    private Slider creationBar;
    private Renderer rend;
    // Канва кнопок постройки
    private GameObject canvasCreation;
    private GameObject canvasCreationBar;


    
    void Awake() {
        bpc = GetComponentInParent<BuildingPrefabControl>();
        if (bpc == null)
            Debug.LogError("CreatingBuilding: Cannot find BuildingPrefabControl script");

        //level = playerManager.levelMainBuilding;
        rend = GetComponent<Renderer>();
        canvasCreation = transform.Find("CanvasCreation").gameObject;
        canvasCreationBar = transform.Find("CanvasCreationBar").gameObject;
        if (canvasCreation == null) {
            Debug.LogError("CreationBuilding: Canvas Creation not found");
            return;
        }

        if (canvasCreationBar == null) {
            Debug.LogError("CreationBuilding: Canvas Creation Bar not found");
            return;
        }

        rend.material = ghostMaterial;
        if (!bpc.autoCreation)
            canvasCreation.SetActive(true);
        else
            AcceptCreation();
    }
    
    public void AcceptCreation() {
        canvasCreation.SetActive(false);
        canvasCreationBar.SetActive(true);
        creationBar = canvasCreationBar.GetComponentInChildren<Slider>();
        creationBar.maxValue = bpc.buildingCreationTime;
        creationBar.value = 0;
        int levelCost = GetComponentInParent<BuildingPrefabControl>().GetAdditionCostForLevel(bpc.buildingLevel);
        // Списывается стоимость здания
        // Минус стоимости уровней до необходимого
        playerManager.CmdDecreaseMoneyOnServer(bpc.buildingCost + levelCost);

        StartCoroutine("CreateBuildingCoroutine");
    }

    public void CancelCreation() {
        Destroy(gameObject.transform.parent.gameObject);
    }

    IEnumerator CreateBuildingCoroutine() {
        // Это, на самом деле, очень плохо
        // многие проверки надо перенести на сервер
        float increment = 0;
        while (increment < bpc.buildingCreationTime) {
            increment += Time.deltaTime;
            if (increment > bpc.buildingCreationTime)
                increment = bpc.buildingCreationTime;

            creationBar.value = increment;

            yield return null;
        }
        // В этой точке здание уже построено и его никак не удалить
        // Отключаем канву времени постройки
        canvasCreationBar.SetActive(false);
		playerManager.AddNewCreationPanel (transform.position);
        // Подготовка здания
		bpc.CmdInstantiateBuildingObject();
        Destroy(this.gameObject);
        //gameObject.SetActive(false);
    }
}