﻿using UnityEngine;

public interface IButtonAccess
{
	void EnableButton(BuildingControl buildingControl);
}
