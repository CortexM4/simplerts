﻿using UnityEngine;

public interface IEnemyVisible
{
    void ReactionOnEnemy(Collider collider);
}