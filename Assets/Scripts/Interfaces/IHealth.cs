﻿using UnityEngine;

public interface IHealth
{
    int AdditionalCostForShoot { get; set; }
	bool IsDestroyed();
	void TakeDamage(int amount, Vector3 hitPoint);
}