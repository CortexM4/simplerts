﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

interface IShootable 
{
    void Shoot(Vector3 shootDir);
    float GetShootDistance();
    List<Aim> GetAims();
}
