﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.Networking;


public class UnitControl : NetworkBehaviour, IPlayer {

	[Serializable]
	public struct Position {
		public float x;
		public float z;
	}

    [SyncVar]
	[HideInInspector] public GameObject playerManagerGo;
    [HideInInspector] public PlayerManager PlayerManager {
        get { return playerManagerGo.GetComponent<PlayerManager>(); }
    }

    public float timeCreation = 2;
    public int unitCost = 100;
    public int invisibleTime = 0;

    protected UnitHealth unitHealth;
    protected UnityEngine.AI.NavMeshAgent navMeshAgent;
    protected bool walking = false;
    protected PlayerManager playerManager;
    private Renderer rend;
    private Material mat;
    private Color defaultColor;

    public override void OnStartServer() {
        base.OnStartServer();
    }

    public override void OnStartClient() {
        base.OnStartClient();

        playerManager = playerManagerGo.GetComponent<PlayerManager>();
        this.gameObject.layer = playerManager.playerLayer;
        defaultColor = playerManager.playerColor;
        rend = GetComponentInChildren<Renderer>();
        mat = rend.material;
        mat.color = defaultColor;

		MiniMapControl.Instance.EnableUnitIcon(transform);
    }

	public override void OnNetworkDestroy() {
		if (!isClient)
			return;
		MiniMapControl.Instance.DisableUnitIcon(transform);
	}

    //----------- Всякие первичные настройки ------------
    public void PrepareUnit(GameObject _playerManager) {
        playerManagerGo = _playerManager;
        playerManager = playerManagerGo.GetComponent<PlayerManager>();

        this.gameObject.layer = playerManager.playerLayer;

        defaultColor = playerManager.playerColor;
        rend = GetComponentInChildren<Renderer>();
        mat = rend.material;
        mat.color = defaultColor;
    }

    protected virtual void Update() {
        if (!isServer)
            return;

        if (unitHealth.IsDestroyed()) {
            navMeshAgent.enabled = false;
            return;
        }

        // ??????????? Нах надо
        if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance) {
            if (!navMeshAgent.hasPath || Mathf.Abs(navMeshAgent.velocity.sqrMagnitude) < float.Epsilon) {
                walking = false;
            }
        }
    }
//------ Передвижение юнита -------------
    public void MoveTo(Vector3 coord) {
        if (!unitHealth.IsDestroyed()) {
            walking = true;
            //unitUniton.gameObject.SetActive(false);
            navMeshAgent.destination = coord;
            navMeshAgent.Resume();
        }
    }

//----- Снять выделение с юнита ----------
    public void UnselectedUnit() {

        // Почему я сделал так, а в BuildingControl по другому,
        // потому что, почти все юниты будут иметь DrawCircle, а строения лишь немногие.
        DrawCircle dc = GetComponentInChildren<DrawCircle>();
        if (dc != null)
            dc.DrawDisable();
        mat.color = defaultColor;
    }

//------ Выделить юнита ----------------
    public void SelectUnit() {
        DrawCircle dc = GetComponentInChildren<DrawCircle>();
        if (dc != null)
            dc.DrawEnable();
        // Пока пусть меняет цвет, эта опция не влияет на игровой процесс
        // в дальнейшем надо добавить анимацию или звук
        mat.color = Color.green;
    }

//-------- Возвращает время создания юнита --------
    public float GetTimeUnitCreation() {
        return timeCreation;
    }

//-------- Возвращает стоимость юнита --------
    public int GetCostForUnit() {
        return this.unitCost;
    }
}
