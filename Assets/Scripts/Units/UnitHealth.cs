﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

public class UnitHealth : NetworkBehaviour, IHealth
{

    public int armor = 0;
    public int startHealth = 100;
    [SyncVar(hook = "OnChangeCurrentHealth")]
	public int currentHealth = 100;                          // The current health the enemy has.

    public int AdditionalCostForShoot { get; set; }

    private bool isDead = false;                                // Whether the enemy is dead.
    private CapsuleCollider capsuleCollider;            // Reference to the capsule collider.
    private Slider healthSlider;


    void OnChangeCurrentHealth(int currentHealth) {
        healthSlider.value = currentHealth;
    }
    // Use this for initialization
    public override void OnStartClient() {
        
        // Setting up the references.
        healthSlider = GetComponentInChildren<Slider>();
        //anim = GetComponent<Animator>();
        //enemyAudio = GetComponent<AudioSource>();
        //hitParticles = GetComponentInChildren<ParticleSystem>();
        capsuleCollider = GetComponent<CapsuleCollider>();


        healthSlider.maxValue = startHealth;
        healthSlider.value = currentHealth;

    }

    public override void OnStartServer() {
        base.OnStartServer();
        capsuleCollider = GetComponent<CapsuleCollider>();
    }

    /// <summary>
    /// Return true if unit is dead
    /// </summary>
    /// <returns></returns>
    public bool IsDestroyed() {
        return isDead;
    }

    public void TakeDamage(int amount, Vector3 hitPoint) {
        
        if (!isServer)
            return;
        // If the enemy is dead...
        if (isDead)
            // ... no need to take damage so exit the function.
            return;

        // Play the hurt sound effect.
        //enemyAudio.Play();

        // Reduce the current health by the amount of damage sustained.
        currentHealth -= amount;

        // Чтобы небыло ошибок, надо проинициализировать healthSlider на сервере
        // healthSlider.value = currentHealth;

        // Set the position of the particle system to where the hit was sustained.
        //hitParticles.transform.position = hitPoint;

        // And play the particles.
        //hitParticles.Play();

        // If the current health is less than or equal to zero...
        if (currentHealth <= 0) {
            // ... the enemy is dead.
            Death();
        }
    }

    void Death() {
        // The enemy is dead.
        isDead = true;

        // Turn the collider into a trigger so shots can pass through it.
        capsuleCollider.isTrigger = true;

        // Tell the animator that the enemy is dead.
        //anim.SetTrigger("Dead");

        // Change the audio clip of the audio source to the death clip and play it (this will stop the hurt clip playing).
        //enemyAudio.clip = deathClip;
        //enemyAudio.Play();
		GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
		
        NetworkServer.Destroy (this.gameObject);
        
        //Destroy(this.gameObject);
    }
}
