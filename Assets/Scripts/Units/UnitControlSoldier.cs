﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class UnitControlSoldier : UnitControl, IEnemyVisible, IVisibility, IShootable
{                                                                                                                                                   //
    //!!!!!!!! Надо выяснить насчет расстояния, т.к. при 1 юнит не может достичь цели и тупит !!!!!!!!!!!
    public float shootDistance = 6;          // Это расстояние с которого начинаем стрелять    // 
    public float shootRate = 1f;
    public ParticleSystem gunParticles;                    // Reference to the particle system.//
    //--------------------------------------------------------------------------------------------

    //-------------- Это относится к классу UnitAttack ---------------------
    public int damagePerShot = 10;
    public float range = 100f;                              // The distance the gun can fire. А это, как далеко полетит снаряд (для рукопашки = shootDistance)
    public int crit = 0;
    public int chanceCrit = 0;
    public int splash = 0;
    private Ray shootRay;                                   // A ray from the gun end forwards.
    private RaycastHit shootHit;                            // A raycast hit to get information about what was hit.
    //----------------------------------------------------------------------

    //private UnitUnion unitUniton;                         // Нужно для объединения. Сейчас использовать не будем. Все, что связанно, в комментарии
    private float nextFire;
    private LayerMask layerMaskEnemy;
    private LayerMask enemyLayer;

    private List<Aim> aims = new List<Aim>();

    public override void OnStartServer() {
        base.OnStartServer();

        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        unitHealth = GetComponent<UnitHealth>();

        // LayerMask, для определения врагов
        enemyLayer = playerManager.enemyLayer;
        layerMaskEnemy = 1 << enemyLayer;

        MoveTo(playerManager.enemySpawnPoint);
    }

    protected override void Update()
    {
        base.Update();

        if (!isServer)
            return;

        if (unitHealth.IsDestroyed())
            return;

        if (Time.time > nextFire) {
            Aim tmp_aim = GetComponent<SelectAim>().GetPrimaryAim();

			if (tmp_aim == null) {
				MoveTo (playerManager.enemySpawnPoint);
			} else {
                EnemyFound (tmp_aim.enemy);
			}
        }
    }

    void EnemyFound(GameObject aim) {

        navMeshAgent.destination = new Vector3(aim.transform.position.x, 0, aim.transform.position.z);                                      // начинаем движение к нему

        if (navMeshAgent.remainingDistance > shootDistance) {                                          // если расстояние до него больше выстрела
            navMeshAgent.Resume();                                                                     // то движемся к нему
            walking = true;
        }
        else if (navMeshAgent.remainingDistance <= shootDistance) {
            transform.LookAt(aim.transform);
            Vector3 dirToShoot = aim.transform.position - transform.position;                    // если расстояние позволяет стрелять, то
            if (Time.time > nextFire) {                                                                 // стреляем с определенным Rate
                nextFire = Time.time + shootRate;
                aim.GetComponent<IHealth>().AdditionalCostForShoot += 1;
                Shoot(dirToShoot);
            }
            navMeshAgent.Stop();
            walking = false;
        }
    }

// ==================================== IShootable интерфейс =================================================
    public List<Aim> GetAims() {
        return aims;
    }
    
    public float GetShootDistance() {
        return shootDistance;
    }

    public void Shoot(Vector3 shootDir) {
        int additionDamage = 0;
        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if (Physics.Raycast(shootRay, out shootHit, range, layerMaskEnemy)) {

            IHealth targetHealth = shootHit.collider.GetComponent<IHealth>();

            // If the EnemyHealth component exist...
            if (targetHealth != null) {
                // ... the enemy should take damage.
                if (Random.Range(0, 101) <= chanceCrit) {
                    // Тут должна быть анимация критического удара, сообщение или звук измещающий о нем
                    additionDamage = damagePerShot * crit / 100;
                }
                targetHealth.TakeDamage(damagePerShot + additionDamage, shootHit.point);
            }
        }

        // ???? Что делать если мы не попадаем или какое-то препятствие на пути

        RpcGunShootParticles();
        gunParticles.Stop();
        gunParticles.Play();
    }
// ==========================================================================================================

    [ClientRpc]
    void RpcGunShootParticles() {
        gunParticles.Stop();
        gunParticles.Play();
    }

// ==================================== IEnemyVisible интерфейс =============================================
    public void ReactionOnEnemy(Collider collider) {
        if (collider.gameObject.layer == enemyLayer) {
            IHealth targetHealth = collider.gameObject.GetComponentInChildren<IHealth>();
            if (targetHealth != null) {
                if (aims.Find(e => e.enemy == collider.gameObject) == null) {
                    Aim aim = new Aim(collider.gameObject);
                    aims.Add(aim);
                }
            }
        }
    }
// ==========================================================================================================
	public bool visibilite = true;
// ==================================== IVisibility интерфейс =============================================
    public bool IsVisible() {
		return visibilite;
    }
// ==========================================================================================================
}