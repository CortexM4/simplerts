﻿using UnityEngine;
using System.Collections;

public class UnitControlWorker : UnitControl, IVisibility
{
    enum stateWorker { moveToResource, CheckResource, FindResource, GetResource, backToMainBuilding, };
    //
    public float workingDuration = 2;       // Время добычи ресурса                                     //
    public float workingDistance = 1f;    // Расстояние, на котором будет работать рабочий от ресурса //

    //--------------------------------------------------------------------------------------------

    private CapsuleCollider cc;                             // Нужно для определения размеров юнита
    private float radius = 0;                               // Радиус коллайдера рабочего


    /// <summary>
    /// Физика перемещения считается на сервере
    /// Потом синхронизируется на клиенты
    /// Может это ошибка, а может и нет
    /// Т.к. юнит управляется сервером, то ресурсы которые он добывает синхронизируются с клиентами
    /// </summary>
    public override void OnStartServer() {
        base.OnStartServer();

        cc = GetComponent<CapsuleCollider>();

        // LayerMask, для ресурсов
        //resourceMask = 1 << LayerMask.NameToLayer("Resources");
        radius = cc.radius;

        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        unitHealth = GetComponent<UnitHealth>();

        //MoveTo(playerManager.enemySpawnPoint);

        StartCoroutine("WorkerCoroutine");
        //else
        //    MoveTo(playerManager.enemySpawnPoint);

    }

    IEnumerator WorkerCoroutine() {
		Resource res = null;
		Vector3 coordResource = Vector3.zero;
		float radiusRes = 0;
		stateWorker state = stateWorker.FindResource;
        
        Vector3 mainBuildingCoord = playerManager.spawnPoint;
        
        int resWorker = 0;
        while (true) {
            switch (state) {
                case stateWorker.moveToResource: {
                        if (unitHealth.IsDestroyed())
                            break;
                        walking = true;
                        navMeshAgent.destination = coordResource;
                        navMeshAgent.Resume();
                        if (Vector3.Distance(transform.position, coordResource) < radiusRes + this.radius + workingDistance) {
                            navMeshAgent.velocity = Vector3.zero;
                            walking = false;
                            navMeshAgent.Stop();
                            state = stateWorker.CheckResource;
                        }
                        break;
                    }
                case stateWorker.CheckResource: {
                        if (unitHealth.IsDestroyed())
                            break;
                        walking = false;
                        if (res != null) {
                            if (res.CheckResource(true))
                                state = stateWorker.GetResource;
                            else
                                state = stateWorker.FindResource;
                        }
                        else
                            state = stateWorker.FindResource;
                        break;
                    }

                case stateWorker.FindResource: {
                        if (unitHealth.IsDestroyed())
                            break;
                        res = playerManager.GetComponent<ResourceManager>().GetNearbyResource(transform.position);
                        if (res != null) {
                            coordResource = res.GetPointOfGathering();
                            radiusRes = res.radius;
                            state = stateWorker.moveToResource;
                        }
                        break;
                    }
                case stateWorker.GetResource: {
                        if (unitHealth.IsDestroyed())
                            break;
                        walking = false;
                        yield return new WaitForSeconds(workingDuration);
                        resWorker += res.GetResource();
                        state = stateWorker.backToMainBuilding;
                        break;
                    }

                // !!!!!!!!!!!! Надо проверить, что MainBuilding существует, иначе кабзда
                case stateWorker.backToMainBuilding: {
                        if (unitHealth.IsDestroyed())
                            break;
                        walking = true;
                        //unitUniton.gameObject.SetActive(false);
                        navMeshAgent.destination = mainBuildingCoord;
                        navMeshAgent.Resume();
                        // !!!!!!!!!!!!!!Тут надо просчитать границы соприкосновения рабочего и ресурса или сделать на траггерах
                        if (Vector3.Distance(transform.position, mainBuildingCoord) < 1) {
                            navMeshAgent.velocity = Vector3.zero;
                            walking = false;
                            navMeshAgent.Stop();
                            playerManager.currentMoney += resWorker;
                            resWorker = 0;
                            state = stateWorker.moveToResource;

                            // Вот эта проверка позволяет "Worker'у" на возыращаться к ресурсу если он был израсходован
                            // а сразу искать новый ближайший ресурс
                            if (res == null) {
                                state = stateWorker.FindResource;
                            }
                        }
                        break;
                    }
            }

            yield return new WaitForSeconds(0.5F);
            //yield return null;
        }
    }

    public bool IsVisible() {
        return true;
    }
}
