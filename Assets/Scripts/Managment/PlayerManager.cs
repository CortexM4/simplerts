﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PlayerManager : NetworkBehaviour {

    enum PlayerAction { None = 0, PlaceAction = 2 }

    // Для directionLight FFF4D6FF
    // В Input переназначить клавиши в соответствии с дефолтными (Submit и Cancel)
    [Header("Network")]

    [Space]
    [SyncVar]
    public string playerName;
    [SyncVar]
    public Color playerColor;							// Определяет цвет игрока
    [SyncVar]
    public int levelMainBuilding = 0;
    [SyncVar]
    public int playerID;                           // ID игрока, получаемое от гейм-мастера
    [SyncVar]
    public int playerLayer;                        // получаем от гейм-мастера свой layer
    [SyncVar]
    public int enemyLayer;                         // layer врага
    [SyncVar]
    public int currentMoney;
    [SyncVar]
    public Vector3 spawnPoint;                      // Координаты постройки главного здания
    [SyncVar]
    public Vector3 enemySpawnPoint;

	public int ping;

    // !ВАЖНО, размеры карты задаются на этапе сборки.
    // Учитывай размеры
    [HideInInspector] public Map playerMap = new Map(128, 32);


    // Перфаб миникарты
    public GameObject miniMapPrefab;
    // Префаб Fog of War
    public GameObject FOWPrefab;
    // Префаб Пользовательско GUI
    public GameObject hudManagerPrefab;
    // Префаб панелей для строительства
    public GameObject creationPanels;
    private List<GameObject> panels = new List<GameObject>();
    // Префаб юнита (рабочего), который будет создаваться первоначально
    public GameObject unitPrefab;
    // Префаб здания, который будет строится
    // Первоначально инициализирован MainBuilding
    public GameObject buildingPrefab;
    // И уровень этого здания
    public int level = 0;

    private PlayerAction playerAction = PlayerAction.None;      // Определяет, какое действие выбрал игрок
    private BuildingControl currentSelectedBuilding;            // Выбранное здание
	private ActionControl actionPrefab;                         // Шаманство, которое будет происходить на поле
    private HUDManager hudManager;                              // HUD конкретного игрокаж
    
    private UnitControl currentSelectedUnit;                    // Выбранные unit'ы в данный момент
    private bool selectingPlaceForInstance = false;

    private bool mouseClickF1 = false;
    private bool isMouseDownFirstTimeF1 = true;     // Флаг отслеживает начало клика для ЛКМ (или тап одним пальцем)

    private bool isDragging = false;
    private Vector3 firstCoordMouseF1;

	[ClientRpc]
	public void RpcPrepareClients() {
        if (!isLocalPlayer)
            return;

        if (hudManagerPrefab == null) {
            Debug.LogError("Assign HUD Manager in inspecor");
            return;
        }
			
        // Добавляем миникарту каждому игроку
        GameObject miniMap = Instantiate(miniMapPrefab);
        miniMap.GetComponent<MiniMapControl>().PrepareMiniMap(playerLayer);
        // Добавляем fog of war
        //GameObject fogOfWar = Instantiate(FOWPrefab);
        //fogOfWar.GetComponent<FOWManager>().layer = this.playerLayer;
        //fogOfWar.GetComponent<FogOfWar>().layer = this.playerLayer;

        hudManager = Instantiate(hudManagerPrefab).GetComponent<HUDManager>();
        hudManager.PrepareHUD(this);
        hudManager.gameObject.SetActive(true);
        this.gameObject.layer = playerLayer;
		CameraManager.Instance.SetPosition(transform.position - new Vector3(0,0,1));
		// MainBuilding должно создаваться без подтверждения, с нулевым уровнем соответственно сам уровень д.б. нулевым
		CmdCreateBuilding(buildingPrefab.name, level, spawnPoint, true);
        //CmdCreateUnit(unitPrefab.name, spawnPoint + new Vector3(0, 0, -2));

	}

	[Command]
	public void CmdIncreaseMainBuildingLevel() {
		levelMainBuilding++;
	}

    [Command]
    public void CmdDecreaseMoneyOnServer(int value) {
        PlayerManager playerManager = this.gameObject.GetComponent<PlayerManager>();
        if (playerManager == null) {
            Debug.LogError("CmdDecreaseMoneyOnServer: playerManager is null");
            return;
        }
        playerManager.currentMoney -= value;
    }

    [Command]
    public void CmdCreateUnit(string unitName, Vector3 position) {
        GameObject unitGo = (GameObject)Instantiate(Resources.Load(unitName, typeof(GameObject)), position, Quaternion.identity);
        if (unitGo == null && this.gameObject) {
            Debug.Log("Ops. Something wrong!");
        }
        unitGo.GetComponent<UnitControl>().PrepareUnit(this.gameObject);
        NetworkServer.SpawnWithClientAuthority(unitGo, this.gameObject);
        //NetworkServer.Spawn(unitGo);
    }

    [Command]
    void CmdCreateBuilding(string buildingName, int level, Vector3 point, bool autoCreation) {
        GameObject buildingGo = (GameObject)Instantiate(Resources.Load(buildingName, typeof(GameObject)), point, Quaternion.identity);
        BuildingPrefabControl bfc = buildingGo.GetComponent<BuildingPrefabControl>();
        
        bfc.PrepareCreationBuilding(this.gameObject, level, autoCreation);
        NetworkServer.SpawnWithClientAuthority(buildingGo, this.gameObject);
    }

    [Command]
    void CmdSelectedAction(string actionName, Vector3 position) {
        GameObject actionGo = (GameObject)Instantiate(Resources.Load(actionName, typeof(GameObject)), position, Quaternion.identity);
        actionGo.GetComponent<ActionControl>().PrepareAction(this.gameObject);
        NetworkServer.SpawnWithClientAuthority(actionGo, this.gameObject);
    }

    // Update is called once per frame
    void Update() {
        if (!isLocalPlayer)
            return;

        PlatformControl();
      
        MouseLeftButtonControl();
    }

    void MouseLeftButtonControl() {
        // Если кнопка мыши зажата
		if (mouseClickF1) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {

                if (isMouseDownFirstTimeF1) {

                    firstCoordMouseF1 = hit.point;
                    isMouseDownFirstTimeF1 = false;

                    if (hit.transform.gameObject.layer == playerLayer) {
                        //isMouseDownFirstTimeF1 = false;
                        if (hit.collider.CompareTag("PanelForCreation")) {
                            Vector3 pointOfCreation = new Vector3((int)hit.transform.position.x, 0, (int)hit.transform.position.z);
                            CmdCreateBuilding(buildingPrefab.name, level, pointOfCreation, false);
                            DisableCreationPanels();
                        }
                        if (hit.collider.CompareTag("Unit")) {                                                  // если юнит, то выбираем
                            UnselectAll();
                            currentSelectedUnit = hit.collider.gameObject.GetComponent<UnitControl>();
                            currentSelectedUnit.SelectUnit();
                        }
                        if (hit.collider.CompareTag("Building")) {
                            UnselectAll();
                            currentSelectedBuilding = hit.collider.gameObject.GetComponent<BuildingControl>();
                            currentSelectedBuilding.SelectedBuilding();
                        }
                    }
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Ground")) {
                        if (selectingPlaceForInstance) {
                            switch (playerAction) {
                                case PlayerAction.PlaceAction: {
                                        Vector3 pointOfCreation = new Vector3(hit.point.x, 0, hit.point.z);
                                        CmdSelectedAction(actionPrefab.name, pointOfCreation);
                                        selectingPlaceForInstance = false;
                                        playerAction = PlayerAction.None;
                                        break;
                                    }
                            }
                        }
                    }
                }
                else {
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Ground")) {
                        // Если расстояние между начальным положением мыши и текущим меняется,
                        // то пользователь перетаскивает мышь
                        if (Vector3.Distance(firstCoordMouseF1, hit.point) > 0) {
                            if (!MiniMapControl.Instance.mouseClick && !MiniMapControl.Instance.mouseIsEnter) {
                                isDragging = true;
                                CameraManager.Instance.MoveCamera(hit.point - firstCoordMouseF1);
                            }
                        }
                    }
                }
            }
		}
        //
        // Если кнопка отпущена, но она была до этого нажата
        else if (!mouseClickF1 && !isMouseDownFirstTimeF1) {
            isMouseDownFirstTimeF1 = true; 
            // И если пользователь перетаскивал зажатую мышь
            if (isDragging) {
                isDragging = false;                     // Сбрасываем флаги и останавливаем камеру
                CameraManager.Instance.StopCamera();
                return;                                 // И выходим
            }
        }
	}

    private void EnableCreationPanels() {
        foreach (GameObject m_panel in panels) {
            m_panel.GetComponent<CheckPanel>().EnablePanels();
        }
    }

    private void DisableCreationPanels() {
        foreach (GameObject m_panel in panels) {
            m_panel.GetComponent<CheckPanel>().DisablePanels();
        }
    }

    /*
     *  В этом методе будет обрабатываться управление с разных устройств
     *  в основном для мобил
     *  http://docs.unity3d.com/ScriptReference/EventSystems.EventSystem.IsPointerOverGameObject.html
     *  Это обязательно надо изучить
     */
    void PlatformControl() {
        //if (!EventSystem.current.IsPointerOverGameObject()) {
            // Перемещение камеры, управление ресурсами
            mouseClickF1 = Input.GetButton("Fire1");
        //}

    }

    public void UnselectAll() {
        selectingPlaceForInstance = false;
        playerAction = PlayerAction.None;
        DisableCreationPanels();
        if (currentSelectedBuilding != null) {
            currentSelectedBuilding.UnselectedBuilding();
            currentSelectedBuilding = null;
        }
        if (currentSelectedUnit != null) {
            currentSelectedUnit.UnselectedUnit();
            currentSelectedUnit = null;
        }
    }

    // -------- Функции общего пользования
    public void CreateMyBuilding(GameObject _buildingPrefab, int _level) {
        buildingPrefab = _buildingPrefab;
        level = _level;
        EnableCreationPanels();
    }

	public void AddNewCreationPanel(Vector3 point) {
		playerMap.CreateBuilding((int)point.x, (int)point.z);
		GameObject panel = (GameObject)Instantiate(creationPanels, point + new Vector3(0, .1f, 0), Quaternion.identity);
		panel.GetComponent<CheckPanel>().SetPlayerLayer(playerLayer);
		panel.GetComponent<CheckPanel>().map = playerMap;
		panels.Add(panel);
	}

	public void DeleteCreationPanel(Vector3 point) {
		GameObject panel = panels.Find (p => p.transform.position.x == point.x && p.transform.position.z == point.z);
		panels.Remove (panel);
		Destroy (panel);
		playerMap.DestroyBuilding((int)point.x, (int)point.z);
	}

	public void ActionInWork(ActionControl _actionPrefab) {
		actionPrefab = _actionPrefab;
        playerAction = PlayerAction.PlaceAction;
        selectingPlaceForInstance = true;
    }

    public HUDManager GetHUD() {
        return hudManager;
    }

}
