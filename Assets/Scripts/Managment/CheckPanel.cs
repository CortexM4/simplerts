﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckPanel : MonoBehaviour {
    public Map map;
    public List<GameObject> panels;

	private bool isEnabled = false;

    public void SetPlayerLayer(LayerMask layer) {
        foreach(GameObject panel in panels)
            panel.layer = layer;
    }

    public void EnablePanels() {
		if (isEnabled)
			return;
		isEnabled = true;

		//Debug.Log ("Ebable Panel( x:" + transform.position.x + " z:" + transform.position.z + ")");
        foreach (GameObject panel in panels) {
            int x = (int)(panel.transform.position.x);
            int z = (int)(panel.transform.position.z);

			//Debug.Log ("x: "+x+ " z: "+ z+" v: "+map [x, z]);
            map[x, z] -= map.m_cost_panel;
			if (map [x, z] == 0) {
				panel.SetActive (true);
			}
				
        }
    }

    public void DisablePanels() {
		if (!isEnabled)
			return;
		isEnabled = false;

		//Debug.Log ("Disable Panel( x:" + transform.position.x + " z:" + transform.position.z + ")");
        foreach (GameObject panel in panels) {	
            int x = (int)(panel.transform.position.x);
            int z = (int)(panel.transform.position.z);

			//Debug.Log ("x: "+x+ " z: "+ z+" v: "+map [x, z]);
            map[x, z] += map.m_cost_panel;
            if (panel.activeSelf) {
                panel.SetActive(false);
            }

        }
    }
	
}
