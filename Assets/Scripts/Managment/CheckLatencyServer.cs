﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CheckLatencyServer : NetworkBehaviour {

	public string ip = "93.158.134.3";
    private Ping ping;

    public override void OnStartLocalPlayer() {
        base.OnStartLocalPlayer();
        ping = new Ping(ip);
		//StartCoroutine ("CheckLatency");
    }

    IEnumerator CheckLatency() {
        while (true) {
			GetComponent<PlayerManager> ().ping = 0;
            if (ping.isDone) {
				// Может это была ошибка сборки, но первый раз у меня клиент не завелся!
				// Надо посмотреть за поведением
				GetComponent<PlayerManager> ().ping = ping.time;
            }

            yield return null;
        }
    }
}
