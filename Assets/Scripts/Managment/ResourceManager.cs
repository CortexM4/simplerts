﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

/// <summary>
/// Хоть класс является компанентом PlayerManager, но исполняться он должен на сервре,
/// т.к. контроль за ресурсами определяется на сервере
/// </summary>
public class ResourceManager : NetworkBehaviour
{
	public int countAllResources = 1;
    public float radiusSearchingResources = 20f;

    public List<Resource> resourceList;

    private string resourceLayerName = "Resources";
    private LayerMask resourceLayer;

    public override void OnStartServer() {
        base.OnStartServer();
        resourceList = new List<Resource>();
        resourceLayer = 1 << LayerMask.NameToLayer(resourceLayerName);

        // Дожидаемся, пока загрузятся "ресурсы" (те, которые собирать надо)
        // и добавляем их в список
        StartCoroutine("WaitLoadingResources");
    }

    public void RemoveResource(Resource res) {
        if (!isServer)
            return;

        //int x = (int)res.transform.position.x;
        //int y = (int)res.transform.position.z;
        // !!!! GameMaster.Instance.SetResourceMap(x, y, resursRadius, resursWeight); Вернуть и изменить
        resourceList.Remove(res);
    }

    public Resource GetNearbyResource(Vector3 point) {
        if (!isServer)
            return null;

        float distance = Mathf.Infinity;
        float currDist;
        Resource tmp_res = null;
        foreach (Resource res in resourceList) {
            if (res == null) {
                // Эта ситуация может происходить тогда, когда один ресурс используется несколькими ResourceManager'ами
                // Из одного списка RM он удаляется, а другой не знает об этом. Происходит ошибка
                // В конечном варианте таких ситуаций быть не должно, т.к. не д.б. ресурсов разделяемых между несколькими RM
                continue;
            }
            currDist = Vector3.Distance(res.GetPointOfGathering(), point);
            if (res.CheckResource(false)) {
                if (currDist < distance) {
                    distance = currDist;
                    tmp_res = res;
                }
            }
        }
        return tmp_res;
    }

    IEnumerator WaitLoadingResources() {
        bool resIsLoaded = false;
        while (!resIsLoaded) {
            Collider[] colRes = Physics.OverlapSphere(transform.position, radiusSearchingResources, resourceLayer);
            if (colRes.Length == countAllResources) {
                foreach (Collider res in colRes) {
                    Resource tmp_res = res.gameObject.GetComponent<Resource>();
                    tmp_res.linkToResourceManager = this;
                    resourceList.Add(tmp_res);
					// !!!! GameMaster.Instance.SetResourceMap(x, y, resursRadius, -resursWeight); Вернуть и изменить
                }
                resIsLoaded = true;
            }
            yield return null;
        }
    }
}
