﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class FOWManager : MonoBehaviour {

	static public FOWManager Instance;
	public int widthHeight = 256;
	public Texture2D generatedTexture;
    [HideInInspector]public LayerMask layer;

	private Material currentMaterial;
	private Color32[] pixels;
	public List<Entity> entitiyes = new List<Entity> ();
	int count = 0;

	void Awake() {
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		if (!currentMaterial) {
			currentMaterial = GetComponent<Renderer> ().sharedMaterial;
			if (currentMaterial == null) {
				Debug.LogWarning ("Cannot find a material on: " + transform.name);
			} else {
				InitializeTexture ();
			}
		}
	}

	private void InitializeTexture() {
		generatedTexture = new Texture2D (widthHeight, widthHeight);
		pixels = generatedTexture.GetPixels32 ();

		currentMaterial.SetTexture ("_FOWTex", generatedTexture);
	}

	// Update is called once per frame
	void LateUpdate () {
		if (count >= 5) {
			count = 0;
            for (int i = 0; i < pixels.Length; i++) {
                pixels[i] = new Color32(0, 0, 0, 255);
            }

            foreach (Entity entity in entitiyes) {
                RevealFOW(entity.transform, entity.Radius);
            }

            generatedTexture.SetPixels32 (pixels);
            generatedTexture.Apply();

		}
		count++;
	}

	void RevealFOW(Transform pos, int radius) {
		Vector3 screenPos = Camera.main.WorldToScreenPoint (pos.position);

		Ray ray = Camera.main.ScreenPointToRay (screenPos);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, 100, 1 << 2)) {
			//int intX = (int)(hit.textureCoord.x * 1000000);
			//int intY = (int)(hit.textureCoord.y * 1000000);
			//float tx = (float)intX / 1000000;
			//float ty = (float)intY / 1000000;
			//Debug.Log( tx * widthHeight + " " + ty * widthHeight);
			int x0 =  (int)(hit.textureCoord.x * widthHeight);
			int y0 =  (int)(hit.textureCoord.y * widthHeight);

			int yMin = y0 - radius;
			int yMax = y0 + radius;
			int xMin = x0 - radius;
			int xMax = x0 + radius;

			Vector2 intlPos = new Vector2 (x0, y0);
			float range2 = (radius * radius);

			if (xMin < 0) xMin = 0; else if (xMax >= generatedTexture.width) xMax = generatedTexture.width - 1;
			if (yMin < 0) yMin = 0; else if (yMax >= generatedTexture.height) yMax = generatedTexture.height - 1;
			if (xMax < xMin) xMax = xMin;
			if (yMax < yMin) yMax = yMin;

			for (int y = yMin; y <= yMax; y++) {
				for (int x = xMin; x <= xMax; x++) {
					var nodePos = new Vector2 (x, y);

					float dist2 = (intlPos - nodePos).sqrMagnitude;

					if (dist2 > range2)
						continue;

					int p = x + y * generatedTexture.width;
					pixels [p].a = 0;
				}
			}
		}
	}
}
