﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;


enum TypeIcon { unit, building };
/// <summary>
/// Minimap Image position and pivot located on Top Center
/// All maths based on it
/// </summary>
public class MiniMapControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler, IDragHandler{


    static public MiniMapControl Instance { get; private set; }

	public RectTransform miniMapTransform;
	public GameObject unitIcon;
	public GameObject buildingIcon;
    public Color playerIconColor = Color.green;
    public Color enemyIconColor = Color.red;

    private LayerMask m_playerLayer;
	private float factorX;
    private float factorZ;
	private int m_rectWidth;
	private int m_rectHeight;
	private float xTerrainSize;
	private float zTerrainSize;

    private List<Icon> iconList = new List<Icon>();

	[HideInInspector] public bool mouseIsEnter = false;
    [HideInInspector] public bool mouseClick = false;

    void Start() {

        Instance = this;

        Terrain terr = FindObjectOfType<Terrain>();
        if (terr == null) {
            Debug.LogError("MinimapControl: Terrain not found");
            return;
        }
        xTerrainSize = terr.terrainData.size.x;
        zTerrainSize = terr.terrainData.size.z;

        if (miniMapTransform == null) {
            Debug.LogError("MinimapControl: Please, assign miniMap RectTransform from Inspecor");
            return;
        }
        m_rectWidth = (int)miniMapTransform.rect.width;
        m_rectHeight = (int)miniMapTransform.rect.height;

        factorX = xTerrainSize / m_rectWidth;
        factorZ = zTerrainSize / m_rectHeight;
    }

    public void PrepareMiniMap(LayerMask layer) {
        m_playerLayer = layer;
    }

	void FixedUpdate() {
        //iconList.RemoveWhere(e => e.relatedTransform == null);
        foreach (Icon icon in iconList) {
            if (icon.relatedTransform == null)
                break;
			if (icon.type == TypeIcon.unit) {
				icon.relatedIcon.transform.localPosition = TranslateToMinimapCoordinate (icon.relatedTransform.position);
			}
        }
	}

	public void OnPointerEnter(PointerEventData eventData) {
		mouseIsEnter = true;
	}
	public void OnPointerExit(PointerEventData eventData) {
		mouseIsEnter = false;
	}
	public void OnPointerUp(PointerEventData eventData) {
		mouseClick = false;
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (mouseIsEnter && mouseClick) {
			myMouseDown (eventData);
		}
	}
				
	public void OnPointerDown (PointerEventData eventData) {
		mouseClick = true;
		myMouseDown (eventData);
	}
		
	private void myMouseDown(PointerEventData dat)
	{
		Vector2 localCursor;
		var pos1 = dat.position;
		if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(miniMapTransform, pos1,
			null, out localCursor))
			return;

		int xpos = (int)(localCursor.x);
		//int ypos = (int)(localCursor.y);

		if (xpos < 0) xpos = xpos + m_rectWidth / 2;
		else xpos += m_rectWidth / 2;

		//ypos = m_rectHeight + ypos;
		TranslateToCameraCoordinate (xpos);
	}

//--------- Публичные методы для отображения иконок на миникарте ------------------
	public void EnableUnitIcon(Transform unitTransform) {
		EnableIcon (TypeIcon.unit, unitTransform);
	}

	public void DisableUnitIcon(Transform unitTransform) {
		DisableIcon (unitTransform);
	}

	public void EnableBuildingIcon(Transform buildingTransform) {
		EnableIcon (TypeIcon.building, buildingTransform);
	}

	public void DisableBuildingIcon(Transform buildingTransform) {
		DisableIcon (buildingTransform);
	}
//---------------------------------------------------------------------------------

	private void EnableIcon(TypeIcon type, Transform point) {
		GameObject icon;
		if (type == TypeIcon.unit) {
			icon = (GameObject)Instantiate (unitIcon, TranslateToMinimapCoordinate (point.position), Quaternion.identity);
		} else {
			icon = (GameObject)Instantiate (buildingIcon, TranslateToMinimapCoordinate (point.position), Quaternion.identity);
		}
		if (point.gameObject.layer == m_playerLayer) {
			icon.GetComponent<Image>().color = playerIconColor;
        }
        else {
			icon.GetComponent<Image>().color = enemyIconColor;
        }
		icon.transform.SetParent (miniMapTransform.transform, false);
		Icon _icon = new Icon (type, icon, point);
		_icon.relatedIcon.transform.localPosition = TranslateToMinimapCoordinate (_icon.relatedTransform.position);
		iconList.Add(_icon);
	}

	private void DisableIcon(Transform point) {
		Icon icon = iconList.Find(e => e.relatedTransform == point);
        if (icon == null) {
            return;
        }
        Destroy(icon.relatedIcon);
        iconList.Remove(icon);
    }

	private Vector2 TranslateToMinimapCoordinate(Vector3 unitPosition) {
        Vector2 iconPosition = new Vector2(unitPosition.x / factorX, unitPosition.z / factorZ);
        iconPosition.x -= m_rectWidth / 2;
        if (iconPosition.x < -(m_rectWidth / 2)) {
            Debug.Log("TranslateToMinimapCoordinate: Wrong translate: " + iconPosition.x);
            iconPosition.x = -(m_rectWidth / 2);
        }
        if (iconPosition.x > (m_rectWidth / 2)) {
            Debug.Log("TranslateToMinimapCoordinate: Wrong translate: " + iconPosition.x);
            iconPosition.x = m_rectWidth / 2;
        }

        iconPosition.y -= m_rectHeight;
        if (iconPosition.y < -m_rectHeight) {
            Debug.Log("TranslateToMinimapCoordinate: Wrong translate: " + iconPosition.y);
            iconPosition.y = -m_rectHeight;
        }
        if (iconPosition.y > 0) {
            Debug.Log("TranslateToMinimapCoordinate: Wrong translate: " + iconPosition.y);
            iconPosition.y = 0;
        }
        return iconPosition;
    }

    private void TranslateToCameraCoordinate(int xpos) {
        CameraManager cameraManager = Camera.main.GetComponent<CameraManager>();
        if (cameraManager == null) {
            Debug.LogError("MinimapControl: Cannot find main camera");
            return;
        }

        cameraManager.SetPosition(new Vector3(xpos * factorX, 0, zTerrainSize / 2));

    }
}

class Icon
{
	
	public TypeIcon type;
    public GameObject relatedIcon;
    public Transform relatedTransform;

    public Icon(TypeIcon type, GameObject icon, Transform transform) {
		this.type = type;
        relatedIcon = icon;
        relatedTransform = transform;
    }
}
