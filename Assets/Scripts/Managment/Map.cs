﻿
/// <summary>
/// На данный момент карта является лакольной для каждого игрока.
/// На будущее, надо перенести ее на сервер и сделать единой
/// </summary>
public class Map
{
    // !!! Надо быть очень аккуратным с этим типом. Можно легко переполнить и будет уже совсем другое значение
    private int[,] point;

    public int this[int x, int y] 
    {
        get { return point[x, y]; }
        set { point[x, y] = value; }
    }

    public sbyte m_cost_panel = 10;

    private int dAxis = 3;
    
    private sbyte m_cost_building = 100;

    public Map(int xSize, int ySize) {
        point = new int[xSize, ySize];
    }

    /*public void MapZone(int x, int y, int size, int weight) {
        //int iteration = size * 3 - (size - 1);
        int i, j;

        // Т.к. terrain [0..max] включет крайние точки, а созданный массив не включает максимум (max),
        // то, для симмтеричности исключаем ноль из обработки. Поэтому в минимуме край - единица.
        int leftX = (x - size > 0) ? x - size : 1;
        int leftY = (y - size > 0) ? y - size : 1;
        int rightX = ((x + size) < xSize) ? x + size : xSize - 1;
        int rightY = ((y + size) < ySize) ? y + size : ySize - 1;
        for (i = leftY; i <= rightY; i++)
            for (j = leftX; j <= rightX; j++)
                cost[j, i] += weight;
    }*/

    public void CreateBuilding(int x, int y) {
        point[x - dAxis, y - dAxis] += m_cost_panel;
        point[x - dAxis, y] += m_cost_panel;
        point[x - dAxis, y + dAxis] += m_cost_panel;
        point[x, y - dAxis] += m_cost_panel;
        point[x, y] -= m_cost_building;
        point[x, y + dAxis] += m_cost_panel;
        point[x + dAxis, y - dAxis] += m_cost_panel;
        point[x + dAxis, y] += m_cost_panel;
        point[x + dAxis, y + dAxis] += m_cost_panel;
    }

    // Изменение карты игрока при уничтожении здания
    public void DestroyBuilding(int x, int y) {
		point[x - dAxis, y - dAxis] -= m_cost_panel;
		point[x - dAxis, y] -= m_cost_panel;
		point[x - dAxis, y + dAxis] -= m_cost_panel;
		point[x, y - dAxis] -= m_cost_panel;
		point[x, y] += m_cost_building;
		point[x, y + dAxis] -= m_cost_panel;
		point[x + dAxis, y - dAxis] -= m_cost_panel;
		point[x + dAxis, y] -= m_cost_panel;
		point[x + dAxis, y + dAxis] -= m_cost_panel;
    }
}