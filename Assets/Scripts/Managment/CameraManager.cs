﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	public static CameraManager Instance { get; private set; }

    public bool isBuildingMenuOpen = false;

	private float cameraSpeed = 5;
	private Vector3 moveVector;     // Надо будет поэксперементировать с Vector3

	void Awake()
	{
		Instance = this;
	}

	// Update is called once per frame
	void FixedUpdate () {
        if (isBuildingMenuOpen)
            return;
		Vector3 tmpPost = Vector3.Lerp(transform.position, transform.position - moveVector, cameraSpeed * Time.deltaTime);
		transform.position = new Vector3(tmpPost.x, transform.position.y, transform.position.z);
	}

	public void SetPosition(Vector3 point) {
        if (isBuildingMenuOpen)
            return;
		transform.position = new Vector3(point.x, transform.position.y, point.z);
	}

	public void MoveCamera(Vector3 vector)
	{
		moveVector = vector;
	}

	public void StopCamera()
	{
		moveVector = Vector3.zero;
	}
}