﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Resource : NetworkBehaviour
{

    public int currentResources = 500;
    public int partResource = 50;

    private bool canUse = true;
    private SphereCollider sphCol;
    [HideInInspector] public float radius;
    [HideInInspector] public ResourceManager linkToResourceManager;

    // Use this for initialization

    void Start() {
        sphCol = GetComponent<SphereCollider>();
        radius = sphCol.radius;
    }


    public Vector3 GetPointOfGathering() {
        return transform.position;
    }

    /*
     *  Проверка ресурса на занятость
     *  если стоит флаг withUse, то ресус занимается юнитом
     *  если этот флаг false, то возвращает заняты все слоты или нет
     */

    public bool CheckResource(bool withUse) {
        if (canUse) {
            canUse = !withUse;
            return true;
        }
        return false;
    }


    public int GetResource() {
        int retValue = partResource;
        canUse = true;
        if (currentResources > 0)
            currentResources -= partResource;
        else
            retValue = 0;

        if (currentResources <= 0 && this != null && canUse) {
            linkToResourceManager.RemoveResource(this);
            Destroy(gameObject);
        }

        return retValue;
    }

}
