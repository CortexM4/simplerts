﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class LobbyPlayer : MonoBehaviour {

    public int slot;
    public Color color;
    public string namePlayer;

    void Awake() {
        slot = ImitatePlayer.GetSlotPlayer();
        color = ImitatePlayer.GetPlayerColor(slot);
        namePlayer = ImitatePlayer.GetPlayerName(slot);

    }
}
