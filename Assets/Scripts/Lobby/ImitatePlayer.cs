﻿using UnityEngine;
using System.Collections;

public class ImitatePlayer : MonoBehaviour {

    
    static int slot = 0;
    static Color[] colors = new Color[] { Color.blue, Color.yellow };
    static string[] names = new string[] { "Peter", "Inna" };

    public static int GetSlotPlayer() {
        slot++;
        if (slot == 3)
            slot = 1;
        return slot;
    }

    public static Color GetPlayerColor(int id) {
        return colors[id -1];
    }

    public static string GetPlayerName(int id) {
        return names[id - 1];
    }

}
