﻿using UnityEngine;
using System.Collections;

public class Player {

    public GameObject m_Instance;
    public PlayerManager m_PlayerManager;
    public Color Color;
    public string PlayerName { get; set; }
    public int CurrentMoney { get; set; }
    public LayerMask LayerMaskPlayer { get; set; }
    public LayerMask LayerMaskEnemy { get; set; }
    public int PlayerID { get; set; }
    public Vector3 spawnPoint { get; set; }
    public Vector3 enemySpawnPoint { get; set; }
    public int LevelMainBuilding { get; set; }

    public void Setup() {
        m_PlayerManager = m_Instance.GetComponent<PlayerManager>();
        m_PlayerManager.playerID = PlayerID;
        m_PlayerManager.playerColor = Color;
        m_PlayerManager.playerName = PlayerName;
        m_PlayerManager.levelMainBuilding = 0;
        m_PlayerManager.spawnPoint = spawnPoint;
        m_PlayerManager.enemySpawnPoint = enemySpawnPoint;

        LayerMaskPlayer = LayerMask.NameToLayer("Player" + PlayerID);
        LayerMaskEnemy = LayerMask.NameToLayer("Player" + (PlayerID%2 + 1).ToString() );

        m_PlayerManager.playerLayer = LayerMaskPlayer;
        m_PlayerManager.enemyLayer = LayerMaskEnemy;
        m_PlayerManager.currentMoney = CurrentMoney;

        //GameMaster.Instance.CreateBuilding(this);
    }
}
