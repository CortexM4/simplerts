﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectAim : MonoBehaviour {

    public Aim GetPrimaryAim() {
        List<Aim> aims = GetComponent<IShootable>().GetAims();
        Aim m_aim = null;
        int maxCost = 0;

        aims.RemoveAll(e => e.enemy == null);
        aims.RemoveAll(e => e.enemy.GetComponentInChildren<IHealth>().IsDestroyed());

        foreach (Aim aim in aims) {
            aim.Cost = 0;
            if (aim.enemy.GetComponentInChildren<IShootable>() != null) {
                aim.Cost = 1000;
            }
            
            aim.Cost += 100 - (int)Vector3.Distance(transform.position, aim.enemy.transform.position);

            aim.Cost += aim.enemy.GetComponent<IHealth>().AdditionalCostForShoot;

            if (aim.Cost > maxCost) {
                maxCost = aim.Cost;
                m_aim = aim;
            }
        }
        return m_aim;
    }
}
