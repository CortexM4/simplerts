﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {

    public int Radius { get; set; }

    private int m_layer;

	// Use this for initialization
	void Start () {

        Radius = GetComponentInParent<NetworkVisibleChecker>().visRange;
        m_layer = GetComponentInParent<IPlayer>().PlayerManager.playerLayer;

		/*if (FOWManager.Instance.layer == m_layer) {
			FOWManager.Instance.entitiyes.Add(this);
        }*/
	}

    /*void OnBecameVisible() {

        if (FOWManager.Instance.layer == m_layer) {
            FOWManager.Instance.entitiyes.Add(this);
        }
    }

    void OnBecameInvisible() {

        if (FOWManager.Instance.layer == m_layer) {
			//Debug.Log ("Remove!");
            FOWManager.Instance.entitiyes.Remove(this);
        }
    }*/

    void OnDestroy() {
		/*if (FOWManager.Instance.layer == m_layer) {
			//Debug.Log ("Remove!");
			FOWManager.Instance.entitiyes.Remove(this);
        }*/
    }
}
