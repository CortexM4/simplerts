﻿using UnityEngine;

public class Aim
{
    public readonly GameObject enemy;
    public int Cost { get; set; }

    public Aim(GameObject enemy) {
        this.enemy = enemy;
        Cost = 0;
    }
}