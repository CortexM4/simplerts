﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class DrawCircle : MonoBehaviour {

    public Color color = Color.green;
    [Range(3, 256)]
    public int numSegments = 128;

    private LineRenderer lineRenderer;
    private float radius = 1;

    //  Пока будем считать, что scale для объекта по всем осям один и тот же. Иначе надо просчитывать для X и Z осей
    private float scale = 1;

    void Start() {
        InitLineRender();
    }

    public void InitLineRender() {
        
        scale = transform.localScale.x;
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Mobile/Particles/Additive"));
        lineRenderer.SetColors(color, color);
        lineRenderer.SetWidth(0.1f, 0.1f);
        lineRenderer.SetVertexCount(numSegments + 1);
        lineRenderer.useWorldSpace = false;
        lineRenderer.enabled = false;

        radius = GetComponentInParent<IShootable>().GetShootDistance() / scale;

        float deltaTheta = (float)(2.0 * Mathf.PI) / numSegments;
        float theta = 0f;

        for (int i = 0; i < numSegments + 1; i++) {
            float x = radius * Mathf.Cos(theta);
            float z = radius * Mathf.Sin(theta);
            Vector3 pos = new Vector3(x, .1f, z);
            lineRenderer.SetPosition(i, pos);
            theta += deltaTheta;
        }
    }

    public void DrawEnable() {
        lineRenderer.enabled = true;
    }

    public void DrawDisable() {
        lineRenderer.enabled = false;
    }
}
