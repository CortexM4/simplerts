﻿using UnityEngine;
using System.Collections;

public class UIDirectionControl : MonoBehaviour {

    // This class is used to make sure world space UI
    // elements such as the health bar face the correct direction.

    public Vector3 positionCreationMenu = new Vector3(-0.6f, 2, -1);
    public bool isCreationMenu = false;

    //public bool m_UseRelativeRotation = true;       // Use relative rotation should be used for this gameobject?


    private Quaternion m_RelativeRotation;          // The local rotatation at the start of the scene.


    private void Start() {
        if (isCreationMenu) {
            transform.localPosition = positionCreationMenu;
        }

		m_RelativeRotation = Camera.main.transform.rotation;
		transform.rotation = m_RelativeRotation;
    }

	void Update() {
		transform.rotation = m_RelativeRotation;
	}
}
