﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class HUDManager : MonoBehaviour {
    public Text currentMoneyText;
    public Text plID;

    public List<BtnActionControl> actions;

    private PlayerManager playerManager;
	private Sprite spriteButton;
    private Color actionColor;
	private ActionControl actionPrefab;

    void Start() {
        if (currentMoneyText == null)
            Debug.LogError("Assign UI element for HUDManager in Inspecor");
    }

    public void PrepareHUD(PlayerManager _playerManager) {
        playerManager = _playerManager;
        plID.text = "Player ID: " + playerManager.playerID;
    }

	// Update is called once per frame
	void Update () {
		// Изменить на что-то типа if(oldMoney != currMoney) .... и далее по тексту. 
		// Короче, не вызывать каждый кадр
        currentMoneyText.text = "Current money: " + playerManager.currentMoney;
		//txtPing.text = "Ping: " + playerManager.ping;
	}

	public void SelecetAction(Sprite _sprite, Color _actionColor, ActionControl _actionPrefab) {
        foreach (BtnActionControl action in actions) {
            action.ActionButtonSelected();
        }

		actionPrefab = _actionPrefab;
		spriteButton = _sprite;
        actionColor = _actionColor;
    }

    public void UnselectAction(BtnActionControl _action) {
        foreach (BtnActionControl action in actions) {
            if (action != _action)
                action.RestoreActionButtonState();
        }
    }

    public Sprite GetImageForActionButton() {
		return spriteButton;
    }

    public Color GetColorForActionButton() {
        return actionColor;
    }

    public ActionControl GetActionPrefab() {
		return actionPrefab;
    }

    public PlayerManager GetPlayerManager() {
        return playerManager;
    }
}
