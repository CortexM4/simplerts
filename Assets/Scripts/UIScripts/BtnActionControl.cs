﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BtnActionControl : MonoBehaviour {
    enum ButtonActionStates { UnInit = 0, Initialization = 1, ActionInWork = 2, }

    public Image buttonImage;
    private HUDManager hud;
    private ButtonActionStates buttonState = ButtonActionStates.UnInit;
    private ButtonActionStates saveState;
    private PlayerManager playerManager;
    private ActionControl actionPrefab;

	// Use this for initialization
	void Start () {
        if (buttonImage == null)
            Debug.LogError(gameObject.name + " Assign button image");
        hud = GetComponentInParent<HUDManager>();
        playerManager = hud.GetPlayerManager();
	}

    public void BtnEvent_SpellsClick() {
        switch(buttonState)
        {
            case ButtonActionStates.UnInit: 
                { 
                    break; 
                }
            case ButtonActionStates.Initialization: 
                {
                    buttonImage.sprite = hud.GetImageForActionButton();
                    GetComponent<Image>().color = hud.GetColorForActionButton();
					actionPrefab = hud.GetActionPrefab();
                    playerManager.UnselectAll();
                    hud.UnselectAction(this);
					playerManager.currentMoney -= actionPrefab.GetActionCost();
                    buttonState = ButtonActionStates.ActionInWork;
                    break;
                }
            case ButtonActionStates.ActionInWork: 
                {
					playerManager.ActionInWork(actionPrefab);
                    break;
                }
        }

    }

    public void ActionButtonSelected() {
        if (buttonState == ButtonActionStates.UnInit) {
            Color imageBtnColor = buttonImage.color;
            imageBtnColor.a = 255;
            buttonImage.color = imageBtnColor;
        }
        saveState = buttonState;
        buttonState = ButtonActionStates.Initialization;
    }

    public void RestoreActionButtonState() {
        if (saveState == ButtonActionStates.UnInit) {
            Color imageBtnColor = buttonImage.color;
            imageBtnColor.a = 0;
            buttonImage.color = imageBtnColor;
        }
        buttonState = saveState;
    }
}
