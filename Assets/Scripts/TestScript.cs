﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class TestScript : NetworkBehaviour {

    [SyncVar]
    Color color;
    Renderer rend;

    public override void OnStartClient()
    {
 	    base.OnStartClient();
        rend = GetComponent<Renderer>();
        rend.material.color = color;
    }

    public void Setup(Color clr) {
        color = clr;
    }

    
}
