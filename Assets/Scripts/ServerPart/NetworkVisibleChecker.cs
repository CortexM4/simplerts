﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic; 

public class NetworkVisibleChecker : NetworkBehaviour {

	public int visRange = 10;
	public float visUpdateInterval = 1.0f; // in seconds
	/*[HideInInspector]*/ public HashSet<NetworkConnection> m_listObservers;						// List или HashSet? Надо подумать

	private float m_VisUpdateTime;
	private NetworkIdentity myNetId;
    [HideInInspector] public LayerMask layerMaskEnemy;
    [HideInInspector] public LayerMask layerMaskPlayer;

    public override void OnStartServer() {
        base.OnStartServer();
		m_listObservers = new HashSet<NetworkConnection> ();
		myNetId = GetComponent<NetworkIdentity> ();
        layerMaskEnemy = 1 << GetComponent<IPlayer>().PlayerManager.enemyLayer;
        layerMaskPlayer = 1 << GetComponent<IPlayer>().PlayerManager.playerLayer;
    }

    [ServerCallback]
	void Update()
	{
		if (!NetworkServer.active)
			return;

		if (Time.time - m_VisUpdateTime > visUpdateInterval) {
			myNetId.RebuildObservers(false);
			m_VisUpdateTime = Time.time;
		}
	}

	public override bool OnRebuildObservers(HashSet<NetworkConnection> observers, bool initial)
	{

        IHealth health = GetComponent<IHealth>();
        if (health == null)
            return false;
        if (health.IsDestroyed())
            return false;

        Collider[] hits = Physics.OverlapSphere(transform.position, visRange, layerMaskPlayer);
		foreach (var hit in hits)
		{
			// (if an object has a connectionToClient, it is a player)
			var uv = hit.GetComponent<NetworkIdentity>();
			if (uv != null && uv.clientAuthorityOwner != null)
			{
				uv.GetComponent<NetworkVisibleChecker> ().m_listObservers.Add (myNetId.clientAuthorityOwner);
			}
		}

        hits = Physics.OverlapSphere(transform.position, visRange, layerMaskEnemy);
        foreach (var hit in hits) {
            // (if an object has a connectionToClient, it is a player)
            var uv = hit.GetComponent<NetworkIdentity>();
            if (uv != null && uv.clientAuthorityOwner != null) {
				// Мне кажется это бредом! Должно проверяться невидимость самого этого объекта, т.к. мы его добавляем
				// Удивительно, но это правильно работает. Надо будем внимательнее изучить
				if (uv.GetComponent<IVisibility> ().IsVisible ()) { 
					uv.GetComponent<NetworkVisibleChecker> ().m_listObservers.Add (myNetId.clientAuthorityOwner);
                    IEnemyVisible reaction = GetComponentInChildren<IEnemyVisible>();
                    if (reaction != null) {
                        reaction.ReactionOnEnemy(hit);
                    }
				}
                //observers.Add(uv.clientAuthorityOwner);
            }
        }

		foreach (NetworkConnection _netId in m_listObservers) {
			if (_netId != null) {
				observers.Add(_netId);
			}
        }

		m_listObservers.Clear ();
		
		return true;
			
	}

	// called hiding and showing objects on the host
	public override void OnSetLocalVisibility(bool vis)
	{
		SetVis(gameObject, vis);
	}

	static void SetVis(GameObject go, bool vis)
	{
		foreach (var r in go.GetComponents<Renderer>())
		{
			r.enabled = vis;
		}
		for (var i = 0; i < go.transform.childCount; i++)
		{
			var t = go.transform.GetChild(i);
			SetVis(t.gameObject, vis);
		}
	}
}
