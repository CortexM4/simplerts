using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;

// ???????? Т.к. это серверная часть, то может убрать наследование от MonoBehavior
public class GameMaster : NetworkBehaviour
{
    static public GameMaster Instance { get; private set; }

    public int startMoney = 1000;
    public Terrain terrain;
	static public List<Player> players = new List<Player>();

    public Transform[] m_SpawnPoint;

    //private Map[] map = new Map[2];

    // Use this for initialization
    //[ServerCallback]
    void Awake() {
        Instance = this;
        players.Clear();
    }

    [ServerCallback]
    private void Start() {

        Debug.Log("Start server");

        StartCoroutine(GameLoop());

    }

    IEnumerator GameLoop() {
        while (players.Count < 2)
            yield return null;

		//wait to be sure that all are ready to start
		yield return new WaitForSeconds(1.0f);

		// Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
		yield return StartCoroutine(RoundStarting());
    }

	private IEnumerator RoundStarting()
	{
		//we notify all clients that the round is starting
		foreach (Player player in players)
			player.m_PlayerManager.RpcPrepareClients ();

		// Wait for the specified length of time until yielding control back to the game loop.
		yield return new WaitForSeconds(3);
	}
    //------------------------------------------------------------------------------

    public void AddPlayer(GameObject playerManager, int playerID, Color color, string playerName) {

        Player player = new Player();
        player.m_Instance = playerManager;
        player.Color = color;
        player.PlayerName = playerName;
        player.PlayerID = playerID;
        player.spawnPoint = playerManager.transform.position;
        for (var i = 0; i < m_SpawnPoint.Length; i++) {
            if (m_SpawnPoint[i].position != playerManager.transform.position) {
                player.enemySpawnPoint = m_SpawnPoint[i].position;
            }
        }
        player.CurrentMoney = startMoney;
        player.LevelMainBuilding = 0;
        player.Setup();
        players.Add(player);

    }

    public Player GetPlayerByID(int playerID) {
        foreach (Player player in players) {
            if (player.PlayerID == playerID)
                return player;
        }

        return null;
    }

    // Устанавливает зоны запрета ресурсов для обоих игроков.
    // Вызывается из менеджера ресурсов.
    // ? Может вызыват и самого скрипта ресурса ?
    // !!! Может использоваться для читерства
    /*public void SetResourceMap(int x, int y, int size, int weight) {
        foreach (Player player in players) {
            player.PlayerMap.MapZone(x, y, size, weight);
        }
    }*/

    // Имя игрока
    public void SetPlayerName(int playerID, string name) {
        foreach (Player _player in players) {
            if (_player.PlayerID == playerID)
                _player.PlayerName = name;
        }
    }


    // Получить координаты вражеского главного здания
    // Чтобы не читерили надо выполнять на сервере
    public Vector3 GetPointOfEnemyMainBuilding(int playerID) {
        foreach (Player _player in players) {
            if (_player.PlayerID != playerID)
                return _player.spawnPoint;
        }

        // !!! Опять же, мы сюда не должны были попасть!!!!
        return Vector3.zero;
    }

    // Вот это надо будет подумать как реализовать, т.к. стоимости и строения должны храниться на сервере
    public void IncreasMainBuildingLevel(int playerID) {
        foreach (Player _player in players) {
            if (_player.PlayerID == playerID)
                _player.LevelMainBuilding++;
        }
    }

    public int GetMainBuildingLevel(int playerID) {
        foreach (Player _player in players) {
            if (_player.PlayerID == playerID)
                return _player.LevelMainBuilding;
        }
        // !!! Опять же, мы сюда не должны были попасть!!!!
        return -1;
    }

    // Получить и установить деньги игрока
    public int GetCurrentMoney(int playerID) {
        foreach (Player _player in players) {
            if (_player.PlayerID == playerID)
                return _player.CurrentMoney;
        }

        // !!! Опять же, мы сюда не должны были попасть!!!!
        return -1;
    }
    public void SetCurrentMoney(int playerID, int _money) {
        foreach (Player _player in players) {
            if (_player.PlayerID == playerID)
                _player.CurrentMoney = _money;
        }
    }
}


