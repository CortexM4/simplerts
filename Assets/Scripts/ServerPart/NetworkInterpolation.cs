﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

//[NetworkSettings(channel = Channels.DefaultUnreliable)]
public class NetworkInterpolation : NetworkBehaviour {

    internal struct State
    {
		internal double timestamp;
		internal Vector3 poistion;
        internal Quaternion rotation;
    };

    private double m_localServerTimer = 0;
    private double m_localClientTimer = 0;

    // State update time on server
    private float serverUpdateTimestep = 0.2f;
    // Count saved state
    // 0.2 * 20 = 4 сек. - мы можем откатиться до состояния, которое было 4 секунды назад.
    // !!!!!!! Буфер всегда будет накапливаться пока объект существует. 
    // !!!!!!! Если он удален(умер), то последнее состояние действительно не выполнится, но думаю, что это не критично.
    private CircularArray<State> m_localBufState = new CircularArray<State>(20);
    private State m_currentLocalState;

    //Whether we are currently interpolating or not
    private bool _isPositionRotationLerping;
    //The start and finish positions and rotation for the interpolation
    private Vector3 _startPosition;
    private Vector3 _endPosition;
    private Quaternion _startQuaternion;
    private Quaternion _endQuaternion;
    //The Time.time value when we started the interpolation
    private float _timeStartedPositionLerping;
    // The time taken to move from the start to finish positions
    private float timeTakenDuringLerp;

    public override void OnStartServer() {
        base.OnStartServer();
        StartCoroutine("UpdateServer");
    }

    IEnumerator UpdateServer() {
        while (true) {
            base.SetDirtyBit(1u);
            yield return new WaitForSeconds(serverUpdateTimestep);
        }
    }

	public override bool OnSerialize(NetworkWriter writer, bool forceAll)
	{
        if (forceAll) 
        {
            //Debug.Log("Force All");
            m_localServerTimer = Network.time;
            writer.Write((double)m_localServerTimer);
            writer.Write((Vector3)this.transform.position);
            writer.Write((Quaternion)transform.rotation);
            return true;
        }

        bool wroteSyncVar = false;
        if ((base.syncVarDirtyBits & 1u) != 0u) {
            if (!wroteSyncVar) {
                // write dirty bits if this is the first SyncVar written
                writer.WritePackedUInt32(base.syncVarDirtyBits);
                wroteSyncVar = true;
            }

            double tmpTimer = Network.time;

            // Мы будем передавать дельту, т.к. это всего 2(ushort) байта вместо 8(double)
            ushort deltaTime = (ushort)((tmpTimer - m_localServerTimer) * 10000);

            //Debug.Log(Network.time+" Sync data. Delta time: " + deltaTime);
            writer.Write((ushort)deltaTime);
            writer.Write((Vector3)this.transform.position);
            writer.Write((Quaternion)transform.rotation);

            m_localServerTimer = tmpTimer;
        }
        if (!wroteSyncVar) {
            // write zero dirty bits if no SyncVars were written
            writer.WritePackedUInt32(0);
        }
        return wroteSyncVar;
	}

    public override void OnDeserialize(NetworkReader reader, bool initialState) {

        if (initialState) {
            m_localClientTimer = (double)reader.ReadDouble();
            Vector3 position = (Vector3)reader.ReadVector3();
            Quaternion rotation = (Quaternion)reader.ReadQuaternion();
            State newState = new State();
            newState.timestamp = m_localClientTimer;
            newState.poistion = position;
            newState.rotation = rotation;

            m_localBufState.AddFirst(newState);

            m_currentLocalState = newState;
            return;
        }
        int num = (int)reader.ReadPackedUInt32();
        if ((num & 1) != 0) {
            // Мы будем получать дельту, т.к. это всего 2(ushort) байта вместо 8(double)
            ushort deltaTimestamp = (ushort)reader.ReadUInt16();
            m_localClientTimer += (double)deltaTimestamp / 10000;

            // ??? А если тут данные наебнутся(это ведь Unreliable), что тогда? Exception?
            Vector3 position = (Vector3)reader.ReadVector3();
            Quaternion rotation = (Quaternion)reader.ReadQuaternion();
            State newState = new State();
            newState.timestamp = m_localClientTimer;
            newState.poistion = position;
            newState.rotation = rotation;
            m_localBufState.AddFirst(newState);
        }
    }

    void StartPositionRotationLerping() {
        if (isServer)
            return;

        if ((m_localBufState[0].timestamp - m_currentLocalState.timestamp) < 2 * serverUpdateTimestep)
            return;

        // Если мы сюда попали, то m_localBufState[0] != m_currentLocalState => index != 0
        int index = 0;
        for (int i = 0; i < m_localBufState.Length; i++) {
            if (m_localBufState[i].timestamp == m_currentLocalState.timestamp) {
                index = i;
            }
        }

        // Этого не может быть, но все же
        if (index == 0)
            return;

        timeTakenDuringLerp = (float)(m_localBufState[index - 1].timestamp - m_currentLocalState.timestamp);
		//Debug.Log ("Time during lerp: " + timeTakenDuringLerp);
        //_startPosition = transform.position;
        _startPosition = m_currentLocalState.poistion;
        _startQuaternion = m_currentLocalState.rotation;
        _endPosition = m_localBufState[index - 1].poistion;
        _endQuaternion = m_localBufState[index - 1].rotation;
        _timeStartedPositionLerping = Time.time;

        //We set the start position to the current position
        _isPositionRotationLerping = true;

        m_currentLocalState = m_localBufState[index - 1];
    }

	// Update is called once per frame
	void FixedUpdate () {
        if (isServer)
            return;

        //--------------------------------------------------
        StartPositionRotationLerping();
        //-------------------------------------------------


        if (_isPositionRotationLerping) {
            //We want percentage = 0.0 when Time.time = _timeStartedLerping
            //and percentage = 1.0 when Time.time = _timeStartedLerping + timeTakenDuringLerp
            //In other words, we want to know what percentage of "timeTakenDuringLerp" the value
            //"Time.time - _timeStartedLerping" is.
            float timeSinceStarted = Time.time - _timeStartedPositionLerping;
            float percentageComplete = timeSinceStarted / timeTakenDuringLerp;
            //Perform the actual lerping.  Notice that the first two parameters will always be the same
            //throughout a single lerp-processs (ie. they won't change until we hit the space-bar again
            //to start another lerp)
            transform.position = Vector3.Lerp(_startPosition, _endPosition, percentageComplete);

            transform.rotation = Quaternion.Lerp(_startQuaternion, _endQuaternion, percentageComplete);

            //When we've completed the lerp, we set _isLerping to false
            if (percentageComplete >= 1.0f) {
                _isPositionRotationLerping = false;
            }
        }
	}
}

// Это не совсем правильное название, на самом деле тут очередь
public class CircularArray<T>
{
    public T this[int index] {
        get { return list[index]; }
        set { list[index] = value; }
    }
    public int Length { get { return list.Length; } }

    private T[] list;

    public CircularArray(int size) {
        list = new T[size];
    }

    public void AddFirst(T t) {
        for (int i = list.Length - 1; i >= 1; i--) {
            list[i] = list[i - 1];
        }
        list[0] = t;
    }

}

