﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

// Установить NtworkSettings
// [NetworkSettings(channel=1, sendInterval=?)]
public class TransformSync : NetworkBehaviour
{

	[SyncVar(hook="OnLocalRotation")]
	[HideInInspector] Quaternion currentRotation;
    [SyncVar(hook = "OnLocalMovement")]
    [HideInInspector] Vector3 currentCoord;

	public float updateTime = .5f;
    public bool syncRotation = true;
    public bool syncPosition = true;

    //Whether we are currently interpolating or not
    private bool _isPositionLerping;
    private bool _isRotationLerping;
    //The start and finish positions and rotation for the interpolation
    private Vector3 _startPosition;
    private Vector3 _endPosition;
    private Quaternion _startQuaternion;
    private Quaternion _endQuaternion;
    // The time taken to move from the start to finish positions
    private float timeTakenDuringLerp;
    //The Time.time value when we started the interpolation
    private float _timeStartedPositionLerping;
    private float _timeStartedRotationLerping;

    public override void OnStartClient() {
        base.OnStartClient();
        timeTakenDuringLerp = updateTime;
        _startPosition = transform.position;
        _endPosition = transform.position;
    }

	/// <summary>
	/// Синхронизация поворота 
	/// !!!! Т.к. это происходит в одной корутине с обновлением перемещения, то надо убедиться, что значения не обновляются при постоянном повороте !!!!!
	/// </summary>
	/// <param name="currentRotation">Current rotation.</param>
	void OnLocalRotation(Quaternion currentRotation) {
        _endQuaternion = currentRotation;
        StartRotationLerping();
	}

    /// <summary>
    /// Отвечает за перемещение на локальных клиентах
    /// </summary>
    /// <param name="currentCoord">Current coordinate.</param>
    void OnLocalMovement(Vector3 currentCoord) {
        _endPosition = currentCoord;

        transform.position = currentCoord;

        //StartPositionLerping();
    }

    void StartPositionLerping() {
        if (isServer)
            return;

        _timeStartedPositionLerping = Time.time;

        //We set the start position to the current position
        _startPosition = transform.position;
        _isPositionLerping = true;
    }

    void StartRotationLerping() {
        if (isServer)
            return;
        _timeStartedRotationLerping = Time.time;

        //We set the start Quaternion to the current Quaternion
        _startQuaternion = transform.rotation;
        _isRotationLerping = true;
    }

    void FixedUpdate() {
        if (isServer)
            return;
        if (_isPositionLerping) {
            //We want percentage = 0.0 when Time.time = _timeStartedLerping
            //and percentage = 1.0 when Time.time = _timeStartedLerping + timeTakenDuringLerp
            //In other words, we want to know what percentage of "timeTakenDuringLerp" the value
            //"Time.time - _timeStartedLerping" is.
            float timeSinceStarted = Time.time - _timeStartedPositionLerping;
            float percentageComplete = timeSinceStarted / timeTakenDuringLerp;
            //Perform the actual lerping.  Notice that the first two parameters will always be the same
            //throughout a single lerp-processs (ie. they won't change until we hit the space-bar again
            //to start another lerp)
            transform.position = Vector3.Lerp(_startPosition, _endPosition, percentageComplete);

            //When we've completed the lerp, we set _isLerping to false
            if (percentageComplete >= 1.0f) {
                _isPositionLerping = false;
            }
        }

        if (_isRotationLerping) {

            float timeSinceStarted = Time.time - _timeStartedRotationLerping;
            float percentageComplete = timeSinceStarted / timeTakenDuringLerp;

            transform.rotation = Quaternion.Lerp(_startQuaternion, _endQuaternion, percentageComplete);

            if (percentageComplete >= 1.0f) {
                _isPositionLerping = false;
            }
        }
 
    }

    public void SyncRotation() {
        OnLocalRotation(currentRotation);
    }

	public void LookAt(Transform aim){
		transform.LookAt (aim);
	}

	public override void OnStartServer ()
	{
		base.OnStartServer ();
		StartCoroutine ("RotationControl");
	}

	IEnumerator RotationControl() {
		while (true) {
            /*if (currentRotation != transform.rotation) {
                currentRotation = transform.rotation;
            }*/
            if (syncRotation) {
                currentRotation = transform.rotation;
            }
            if (syncPosition) {
                currentCoord = transform.position;
            }
            yield return new WaitForSeconds(updateTime);
		}
	}
}
