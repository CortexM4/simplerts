﻿using UnityEngine;
using System.Collections;

public class PlayerInitialize : MonoBehaviour {

    public static PlayerInitialize Instance { get; private set; }

    // ----------------  Это будем получать из lobby -----------------------
    public int[] ids = new int[] { 1, 2 };
    public string[] names = new string[] { "Bob", "Anna" };
    public Color[] playersColor = new Color[] { Color.red, Color.black };
    [HideInInspector]
    public int i = 0;
    //----------------------------------------------------------------------

    void Awake() {
        Instance = this;
    }

    // ---------- Эти функции тоже используются временно, пока нет lobby ----------
    public int GetPlayerID() {
        i++;
        return ids[i - 1];
    }
    public Color GetColor(int id) {
        return playersColor[id - 1];
    }
    public string GetName(int id) {
        return names[id - 1];
    }
}
