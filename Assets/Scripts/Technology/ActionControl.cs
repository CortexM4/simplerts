﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ActionControl : NetworkBehaviour, IPlayer {
    [SyncVar]
    private GameObject playerManagerObject;
	public int technologyCost = 100;
	protected PlayerManager playerManager;
    [HideInInspector] public PlayerManager PlayerManager {
        get { return playerManager; }
    }
	// Use this for initialization

    public override void OnStartClient() {
        base.OnStartClient();
        playerManager = playerManagerObject.GetComponent<PlayerManager>();
    }

	public void PrepareAction(GameObject _playerManager) {
        playerManagerObject = _playerManager;
        playerManager = playerManagerObject.GetComponent<PlayerManager>();
        this.gameObject.layer = playerManager.playerLayer;
	}


    //-------- Возвращает стоимость действия --------
    public int GetActionCost() {
        return technologyCost;
    }    
}
