﻿using UnityEngine;
using System.Collections;

public class ParticleSystemsManagment : MonoBehaviour {

	private ParticleSystem[] systems;
	private bool isDone = false;

	// Use this for initialization
	void Start () {
		systems = GetComponentsInChildren<ParticleSystem> ();


	}
	
	// Update is called once per frame
	void Update () {
		isDone = false;
		foreach (ParticleSystem system in systems) {
			isDone |= system.IsAlive (true);
		}

		if (!isDone)
			Destroy (gameObject);
	
	}

	public void Explosion() {
		foreach (ParticleSystem system in systems) {
			system.Clear ();
			system.Play ();
		}
	}
}
