﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MineControl : ActionControl, IEnemyVisible, IVisibility {

	public GameObject explosion;
	public float timeBeforeExplosion = 1;
	public float radiusExplosion = 5;
	public int explosionDamage = 10;

    private bool m_visibility = false;
	private LayerMask layerMaskEnemy;
	private LayerMask layerEnemy;

	void Start() {
		layerEnemy = playerManager.enemyLayer;
        layerMaskEnemy = 1 << layerEnemy;
	}

	void OnTriggerEnter(Collider col) {
		if (!isServer)
			return;
        if (col.gameObject.layer == layerEnemy) {
            m_visibility = true;
            StartCoroutine("Explosion");
        }
	}

	IEnumerator Explosion() {
		yield return new WaitForSeconds (timeBeforeExplosion);

		
		Collider[] hitCollider = Physics.OverlapSphere(transform.position, radiusExplosion, layerMaskEnemy);
		foreach (Collider col in hitCollider) {
			// Смысл в том, что функция TakeDamage принимает урон и точку от RayCast'a до цели
			// У мины пока поставил Vector3.zero
			col.GetComponentInChildren<IHealth> ().TakeDamage (explosionDamage, Vector3.zero);
		}
        RpcExplosion();
		Destroy (gameObject);
	}

    [ClientRpc]
    void RpcExplosion() {
        Instantiate(explosion, transform.position, Quaternion.identity);
    }

    public void ReactionOnEnemy(Collider collider) {
    }

    public bool IsVisible() {
        return m_visibility;
    }
}
