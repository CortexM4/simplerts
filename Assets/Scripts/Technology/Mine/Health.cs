﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour, IHealth {

	public int startingHealth = 10;            // The amount of health the enemy starts the game with.
	private int currentHealth;                   // The current health the enemy has.

    public int AdditionalCostForShoot { get; set; }

	private bool isDestroy = false;                                // Whether the enemy is dead.
	private SphereCollider sphereCollider;            // Reference to the capsule collider.

	[HideInInspector] public int playerID;

	// Use this for initialization
	void Awake() {
		// Setting up the references.
		//healthSlider = GetComponentInChildren<Slider>();
		//anim = GetComponent<Animator>();
		//enemyAudio = GetComponent<AudioSource>();
		//hitParticles = GetComponentInChildren<ParticleSystem>();
		sphereCollider = GetComponent<SphereCollider>();
		currentHealth = startingHealth;

	}
		
	public bool IsDestroyed() {
		return isDestroy;
	}

	public void TakeDamage(int amount, Vector3 hitPoint) {
		// If the enemy is dead...
		if (isDestroy)
			// ... no need to take damage so exit the function.
			return;

		// Play the hurt sound effect.
		//enemyAudio.Play();

		// Reduce the current health by the amount of damage sustained.
		currentHealth -= amount;

		// Set the position of the particle system to where the hit was sustained.
		//hitParticles.transform.position = hitPoint;

		// And play the particles.
		//hitParticles.Play();

		// If the current health is less than or equal to zero...
		if (currentHealth <= 0) {
			// ... the enemy is dead.
			DestroyObject();
		}
	}

	void DestroyObject() {
		// The enemy is dead.

		isDestroy = true;

		// Turn the collider into a trigger so shots can pass through it.
		sphereCollider.isTrigger = true;

		// Tell the animator that the enemy is dead.
		//anim.SetTrigger("Dead");

		// Change the audio clip of the audio source to the death clip and play it (this will stop the hurt clip playing).
		//enemyAudio.clip = deathClip;
		//enemyAudio.Play();

		Destroy(gameObject, 2f);
	}
}
