﻿// Read article in Evernote "Efficient Gaussian blur with linear sampling" for more optimozation

Shader "FogOfWar/FOWShader" {
	Properties {
		_FOWTex ("Albedo (RGB)", 2D) = "white" {}
		_FOWTint ("GUI Tint", Color) = (1,1,1,1)
		_FadeValue ("Fade Value", Range(0,1)) = 1

        _Resolution ("Resolution Texture", float) = 128    
	}
	SubShader {
		Tags { "Queue" = "Transparent"
		 "IgnoreProjector"="True" 
		 "RenderType"="Transparent" 
		 "ForceNoShadowCasting" = "True" }
		ZWrite Off
		Cull Back
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Unlit alpha novertexlights

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _FOWTex;
		float _FadeValue;
		float4 _FOWTint;
		float _Resolution;


		struct Input {
			float2 uv_FOWTex;
		};

		inline fixed4 LightingUnlit (SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten) {
			fixed4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_FOWTex, IN.uv_FOWTex);

			float sum = 0.0;

            float stepPix = 1/_Resolution;
			
			float2 tc = IN.uv_FOWTex; 

			// ----------------- Linear sampling -------------------------------------------
			//sum += tex2D(_FOWTex, float2(tc.x, tc.y)).a * 0.2270270270;
			//sum += tex2D(_FOWTex, float2(tc.x, tc.y - 1.3846153846 * stepPix)).a * 0.3162162162;
            //sum += tex2D(_FOWTex, float2(tc.x, tc.y - 3.2307692308 * stepPix)).a * 0.0702702703;
			//sum += tex2D(_FOWTex, float2(tc.x, tc.y + 3.2307692308 * stepPix)).a * 0.0702702703;
			//sum += tex2D(_FOWTex, float2(tc.x, tc.y + 1.3846153846 * stepPix)).a * 0.3162162162;

			//sum += tex2D(_FOWTex, float2(tc.x - 1.3846153846 * stepPix, tc.y)).a * 0.3162162162;
            //sum += tex2D(_FOWTex, float2(tc.x - 3.2307692308 * stepPix, tc.y)).a * 0.0702702703;
			//sum += tex2D(_FOWTex, float2(tc.x + 3.2307692308 * stepPix, tc.y)).a * 0.0702702703;
			//sum += tex2D(_FOWTex, float2(tc.x + 1.3846153846 * stepPix, tc.y)).a * 0.3162162162;


			// ------------ Gaussian blur (full version) --------------------------------
			sum += tex2D(_FOWTex, float2(tc.x, tc.y)).a * 0.2270270270;

			sum += tex2D(_FOWTex, float2(tc.x, tc.y - 4.0*stepPix)).a * 0.0162162162;
            sum += tex2D(_FOWTex, float2(tc.x, tc.y - 3.0*stepPix)).a * 0.0540540541;
            sum += tex2D(_FOWTex, float2(tc.x, tc.y - 2.0*stepPix)).a * 0.1216216216;
            sum += tex2D(_FOWTex, float2(tc.x, tc.y - 1.0*stepPix)).a * 0.1945945946;
            sum += tex2D(_FOWTex, float2(tc.x, tc.y + 1.0*stepPix)).a * 0.1945945946;
            sum += tex2D(_FOWTex, float2(tc.x, tc.y + 2.0*stepPix)).a * 0.1216216216;
            sum += tex2D(_FOWTex, float2(tc.x, tc.y + 3.0*stepPix)).a * 0.0540540541;
            sum += tex2D(_FOWTex, float2(tc.x, tc.y + 4.0*stepPix)).a * 0.0162162162;

			sum += tex2D(_FOWTex, float2(tc.x - 4.0*stepPix, tc.y)).a * 0.0162162162;
            sum += tex2D(_FOWTex, float2(tc.x - 3.0*stepPix, tc.y)).a * 0.0540540541;
            sum += tex2D(_FOWTex, float2(tc.x - 2.0*stepPix, tc.y)).a * 0.1216216216;
            sum += tex2D(_FOWTex, float2(tc.x - 1.0*stepPix, tc.y)).a * 0.1945945946;
            sum += tex2D(_FOWTex, float2(tc.x + 1.0*stepPix, tc.y)).a * 0.1945945946;
            sum += tex2D(_FOWTex, float2(tc.x + 2.0*stepPix, tc.y)).a * 0.1216216216;
            sum += tex2D(_FOWTex, float2(tc.x + 3.0*stepPix, tc.y)).a * 0.0540540541;
            sum += tex2D(_FOWTex, float2(tc.x + 4.0*stepPix, tc.y)).a * 0.0162162162;
			//----------------------------------------------------------------------------

			o.Albedo = c.rgb * _FOWTint.rgb;
			o.Alpha = sum * _FadeValue;

			//fixed4 c = tex2D (_FOWTex, IN.uv_FOWTex);
			//o.Albedo = c.rgb * _FOWTint.rgb;
			//o.Alpha = c.a * _FadeValue;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
