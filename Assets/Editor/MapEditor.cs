﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

//[CustomEditor(typeof(GameMaster))]
public class PointEditor : Editor
{
    /*private bool gridONP1 = false;
    private bool gridONP2 = false;
    private bool spotPoint = true;*/

    /*void OnSceneGUI() {
        int x, z;
        GameMaster gm = target as GameMaster;
        x = (int)GameMaster.terrain.terrainData.size.x;
        z = (int)GameMaster.terrain.terrainData.size.z;
        int i, j;

        // Карта отображается, т.к. ID игрока известны, но все может накрыться пиздой!!!!!
        if (gridONP1) {
            for (i = 0; i < x; i++)
                for (j = 0; j < z; j++) {
                    if (gm.GetPlayerMapByID(1).Point(i, j) > 0) {
                        Handles.color = Color.green;
                        Handles.DotCap(0, new Vector3(i, 0, j), Quaternion.identity, .1f);
                    }
                    else if (gm.GetPlayerMapByID(1).Point(i, j) < 0) {
                        Handles.color = Color.red;
                        Handles.DotCap(0, new Vector3(i, 0, j), Quaternion.identity, .1f);
                    }
                }
        }
        if (gridONP2) {
            for (i = 0; i < x; i++)
                for (j = 0; j < z; j++) {
                    if (gm.GetPlayerMapByID(2).Point(i, j) > 0) {
                        Handles.color = Color.green;
                        Handles.DotCap(0, new Vector3(i, 0, j), Quaternion.identity, .1f);
                    }
                    else if (gm.GetPlayerMapByID(2).Point(i, j) < 0) {
                        Handles.color = Color.red;
                        Handles.DotCap(0, new Vector3(i, 0, j), Quaternion.identity, .1f);
                    }
                }
        }

        if (spotPoint) {
            for (i = 0; i < GameMaster.spotPoint.Length; i++) {
                EditorGUI.BeginChangeCheck();
                Handles.Label(gm.spotPoint[i] + Vector3.up * 2, gm.spotPoint[i].ToString());
                Vector3 point_tmp = Handles.PositionHandle(gm.spotPoint[i], Quaternion.identity);
                if (EditorGUI.EndChangeCheck()) {
                    Undo.RecordObject(gm, "Move Point");
                    EditorUtility.SetDirty(gm);
                    gm.spotPoint[i] = new Vector3((int)point_tmp.x, 0, (int)point_tmp.z);
                }
            }
        }

        Handles.BeginGUI();
        GUILayout.BeginArea(new Rect(10, 10, 200, 100));
        gridONP1 = GUILayout.Toggle(gridONP1, "Enable Map Player1");
        gridONP2 = GUILayout.Toggle(gridONP2, "Enable Map Player2");
        spotPoint = GUILayout.Toggle(spotPoint, "Enable spot");
        GUILayout.EndArea();
        Handles.EndGUI();
    }*/
}
