﻿Shader "Unlit/Test"
{
	SubShader {
         Tags { "RenderType"="Opaque" }
         Pass {
			ZWrite On		// В GridOverlay  это Off
			ZTest LEqual	// а этого вообще нету
			Cull Off
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha
			BindChannels {
                 Bind "vertex", vertex Bind "color", color
             }
         }
	}
}
